# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 13.0.1 for Linux x86 (64-bit) (January 29, 2022)
# Date: Wed 5 Oct 2022 11:30:05



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# User-defined parameters.
MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = 'm_Z',
               lhablock = 'FRBlock',
               lhacode = [ 1 ])

MW = Parameter(name = 'MW',
               nature = 'external',
               type = 'real',
               value = 79.947,
               texname = 'm_W',
               lhablock = 'FRBlock',
               lhacode = [ 2 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 0.007757951900698216,
                  texname = '\\alpha _w^{-1}',
                  lhablock = 'FRBlock',
                  lhacode = [ 3 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_F',
               lhablock = 'FRBlock',
               lhacode = [ 4 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1172,
               texname = '\\alpha _s',
               lhablock = 'FRBlock',
               lhacode = [ 5 ])

mHsm = Parameter(name = 'mHsm',
                 nature = 'external',
                 type = 'real',
                 value = 125.09,
                 texname = 'm_h',
                 lhablock = 'FRBlock',
                 lhacode = [ 6 ])

mHc = Parameter(name = 'mHc',
                nature = 'external',
                type = 'real',
                value = 200.,
                texname = 'm_{\\text{Hc}}',
                lhablock = 'FRBlock',
                lhacode = [ 7 ])

alph1 = Parameter(name = 'alph1',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\alpha _1',
                  lhablock = 'FRBlock',
                  lhacode = [ 8 ])

alph2 = Parameter(name = 'alph2',
                  nature = 'external',
                  type = 'real',
                  value = -0.1,
                  texname = '\\alpha _2',
                  lhablock = 'FRBlock',
                  lhacode = [ 9 ])

alph3 = Parameter(name = 'alph3',
                  nature = 'external',
                  type = 'real',
                  value = 0.1,
                  texname = '\\alpha _3',
                  lhablock = 'FRBlock',
                  lhacode = [ 10 ])

mH1 = Parameter(name = 'mH1',
                nature = 'external',
                type = 'real',
                value = 100.,
                texname = 'm_{\\text{H1}}',
                lhablock = 'FRBlock',
                lhacode = [ 11 ])

mH2 = Parameter(name = 'mH2',
                nature = 'external',
                type = 'real',
                value = 200.,
                texname = 'm_{\\text{H2}}',
                lhablock = 'FRBlock',
                lhacode = [ 12 ])

m22sq = Parameter(name = 'm22sq',
                  nature = 'external',
                  type = 'real',
                  value = 100,
                  texname = 'm_{22}{}^2',
                  lhablock = 'FRBlock',
                  lhacode = [ 13 ])

mssq = Parameter(name = 'mssq',
                 nature = 'external',
                 type = 'real',
                 value = 100,
                 texname = 'm_s{}^2',
                 lhablock = 'FRBlock',
                 lhacode = [ 14 ])

L2 = Parameter(name = 'L2',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\lambda _2',
               lhablock = 'FRBlock',
               lhacode = [ 15 ])

L6 = Parameter(name = 'L6',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\lambda _6',
               lhablock = 'FRBlock',
               lhacode = [ 16 ])

L8 = Parameter(name = 'L8',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\lambda _8',
               lhablock = 'FRBlock',
               lhacode = [ 17 ])

MC = Parameter(name = 'MC',
               nature = 'external',
               type = 'real',
               value = 1.2,
               texname = 'm_c',
               lhablock = 'FRBlock',
               lhacode = [ 18 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.23,
               texname = 'm_b',
               lhablock = 'FRBlock',
               lhacode = [ 19 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172.5,
               texname = 'm_t',
               lhablock = 'FRBlock',
               lhacode = [ 20 ])

Mnue = Parameter(name = 'Mnue',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{Mnue}',
                 lhablock = 'MASS',
                 lhacode = [ 12 ])

Mnum = Parameter(name = 'Mnum',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{Mnum}',
                 lhablock = 'MASS',
                 lhacode = [ 14 ])

Mnut = Parameter(name = 'Mnut',
                 nature = 'external',
                 type = 'real',
                 value = 0.,
                 texname = '\\text{Mnut}',
                 lhablock = 'MASS',
                 lhacode = [ 16 ])

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.,
               texname = '\\text{Me}',
               lhablock = 'MASS',
               lhacode = [ 11 ])

MM = Parameter(name = 'MM',
               nature = 'external',
               type = 'real',
               value = 0.1057,
               texname = '\\text{MM}',
               lhablock = 'MASS',
               lhacode = [ 13 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MU = Parameter(name = 'MU',
               nature = 'external',
               type = 'real',
               value = 0.,
               texname = 'M',
               lhablock = 'MASS',
               lhacode = [ 2 ])

MD = Parameter(name = 'MD',
               nature = 'external',
               type = 'real',
               value = 0.,
               texname = '\\text{MD}',
               lhablock = 'MASS',
               lhacode = [ 1 ])

MS = Parameter(name = 'MS',
               nature = 'external',
               type = 'real',
               value = 0.,
               texname = '\\text{MS}',
               lhablock = 'MASS',
               lhacode = [ 3 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 2.,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WHsm = Parameter(name = 'WHsm',
                 nature = 'external',
                 type = 'real',
                 value = 1.,
                 texname = '\\text{WHsm}',
                 lhablock = 'DECAY',
                 lhacode = [ 25 ])

WH1 = Parameter(name = 'WH1',
                nature = 'external',
                type = 'real',
                value = 0.,
                texname = '\\text{WH1}',
                lhablock = 'DECAY',
                lhacode = [ 35 ])

WH2 = Parameter(name = 'WH2',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{WH2}',
                lhablock = 'DECAY',
                lhacode = [ 36 ])

WH3 = Parameter(name = 'WH3',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{WH3}',
                lhablock = 'DECAY',
                lhacode = [ 28 ])

WHc = Parameter(name = 'WHc',
                nature = 'external',
                type = 'real',
                value = 1.,
                texname = '\\text{WHc}',
                lhablock = 'DECAY',
                lhacode = [ 37 ])

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEWM1)*cmath.sqrt(cmath.pi)',
               texname = 'e')

CW2 = Parameter(name = 'CW2',
                nature = 'internal',
                type = 'real',
                value = 'MW**2/MZ**2',
                texname = 'c_w^2')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

v = Parameter(name = 'v',
              nature = 'internal',
              type = 'real',
              value = '0.8408964152537146/cmath.sqrt(Gf)',
              texname = 'v')

RR1x1 = Parameter(name = 'RR1x1',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(alph1)*cmath.cos(alph2)',
                  texname = '\\text{RR1x1}')

RR1x2 = Parameter(name = 'RR1x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(alph2)*cmath.sin(alph1)',
                  texname = '\\text{RR1x2}')

RR1x3 = Parameter(name = 'RR1x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sin(alph2)',
                  texname = '\\text{RR1x3}')

RR2x1 = Parameter(name = 'RR2x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(alph3)*cmath.sin(alph1)) - cmath.cos(alph1)*cmath.sin(alph2)*cmath.sin(alph3)',
                  texname = '\\text{RR2x1}')

RR2x2 = Parameter(name = 'RR2x2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(alph1)*cmath.cos(alph3) - cmath.sin(alph1)*cmath.sin(alph2)*cmath.sin(alph3)',
                  texname = '\\text{RR2x2}')

RR2x3 = Parameter(name = 'RR2x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(alph2)*cmath.sin(alph3)',
                  texname = '\\text{RR2x3}')

RR3x1 = Parameter(name = 'RR3x1',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(alph1)*cmath.cos(alph3)*cmath.sin(alph2)) + cmath.sin(alph1)*cmath.sin(alph3)',
                  texname = '\\text{RR3x1}')

RR3x2 = Parameter(name = 'RR3x2',
                  nature = 'internal',
                  type = 'real',
                  value = '-(cmath.cos(alph3)*cmath.sin(alph1)*cmath.sin(alph2)) - cmath.cos(alph1)*cmath.sin(alph3)',
                  texname = '\\text{RR3x2}')

RR3x3 = Parameter(name = 'RR3x3',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.cos(alph2)*cmath.cos(alph3)',
                  texname = '\\text{RR3x3}')

m11sq = Parameter(name = 'm11sq',
                  nature = 'internal',
                  type = 'real',
                  value = '-0.5*mHsm**2',
                  texname = 'm_{11}{}^2')

CKM1x1 = Parameter(name = 'CKM1x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.97428',
                   texname = '\\text{CKM1x1}')

CKM1x2 = Parameter(name = 'CKM1x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.2253',
                   texname = '\\text{CKM1x2}')

CKM1x3 = Parameter(name = 'CKM1x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.00347',
                   texname = '\\text{CKM1x3}')

CKM2x1 = Parameter(name = 'CKM2x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.2252',
                   texname = '\\text{CKM2x1}')

CKM2x2 = Parameter(name = 'CKM2x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.97345',
                   texname = '\\text{CKM2x2}')

CKM2x3 = Parameter(name = 'CKM2x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.041',
                   texname = '\\text{CKM2x3}')

CKM3x1 = Parameter(name = 'CKM3x1',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.00862',
                   texname = '\\text{CKM3x1}')

CKM3x2 = Parameter(name = 'CKM3x2',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.0403',
                   texname = '\\text{CKM3x2}')

CKM3x3 = Parameter(name = 'CKM3x3',
                   nature = 'internal',
                   type = 'complex',
                   value = '0.999152',
                   texname = '\\text{CKM3x3}')

CW = Parameter(name = 'CW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(CW2)',
               texname = 'c_w')

mH3 = Parameter(name = 'mH3',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(-((mH1**2*RR1x1*RR1x2 + mH2**2*RR2x1*RR2x2)/(RR3x1*RR3x2)))',
                texname = 'm_{\\text{H3}}')

SW2 = Parameter(name = 'SW2',
                nature = 'internal',
                type = 'real',
                value = '1 - CW2',
                texname = 's_w^2')

L1 = Parameter(name = 'L1',
               nature = 'internal',
               type = 'real',
               value = 'mHsm**2/v**2',
               texname = '\\lambda _1')

L3 = Parameter(name = 'L3',
               nature = 'internal',
               type = 'real',
               value = '(2*(-m22sq + mHc**2))/v**2',
               texname = '\\lambda _3')

SW = Parameter(name = 'SW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(SW2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/CW',
               texname = 'g_1')

L4 = Parameter(name = 'L4',
               nature = 'internal',
               type = 'real',
               value = '((mH2**2 + mH3**2 - 2*mHc**2)*RR1x3 + 2*(-mH1**2 + mH2**2)*RR1x2*RR2x2*RR2x3 + (-mH1**2 + mH2**2)*RR1x3*RR2x3**2 - 2*(mH2**2 + mH3**2 - 2*mHc**2)*RR2x1*RR3x2 + 2*(mH1 - mH3)*(mH1 + mH3)*RR1x2*RR3x2*RR3x3 + (mH1 - mH3)*(mH1 + mH3)*RR1x3*RR3x3**2)/((RR1x3 - 2*RR2x1*RR3x2)*v**2)',
               texname = '\\lambda _4')

L5 = Parameter(name = 'L5',
               nature = 'internal',
               type = 'real',
               value = '(RR1x3*(mH3**2 - mH1**2*RR2x3**2 + mH2**2*(-1 + RR2x3**2) + (mH1 - mH3)*(mH1 + mH3)*RR3x3**2))/((RR1x3 - 2*RR2x1*RR3x2)*v**2)',
               texname = '\\lambda _5')

L7 = Parameter(name = 'L7',
               nature = 'internal',
               type = 'real',
               value = '(-2*(mH2**2*RR2x3*(2*RR1x2*RR2x2 + RR1x3*RR2x3) + mssq*(RR1x3 - 2*RR2x1*RR3x2) + mH3**2*(RR1x1*RR3x1 - RR1x2*RR3x2)*RR3x3 - mH1**2*(-2*RR2x1*RR3x2 + 2*RR1x2*(RR2x2*RR2x3 - RR3x2*RR3x3) + RR1x3*(1 + RR2x3**2 - RR3x3**2))))/((RR1x3 - 2*RR2x1*RR3x2)*v**2)',
               texname = '\\lambda _7')

Timag = Parameter(name = 'Timag',
                  nature = 'internal',
                  type = 'real',
                  value = '(RR1x2*((-mH1**2 + mH2**2)*RR2x2**2 + (mH1 - mH3)*(mH1 + mH3)*RR3x2**2))/((RR1x3 - 2*RR2x1*RR3x2)*v)',
                  texname = 'T_i')

Treal = Parameter(name = 'Treal',
                  nature = 'internal',
                  type = 'real',
                  value = '(RR1x1*((-mH1**2 + mH2**2)*RR2x1**2 + (mH1 - mH3)*(mH1 + mH3)*RR3x1**2))/((RR1x3 - 2*RR2x1*RR3x2)*v)',
                  texname = 'T_r')

g2 = Parameter(name = 'g2',
               nature = 'internal',
               type = 'real',
               value = 'ee/SW',
               texname = 'g_2')

