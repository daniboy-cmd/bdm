import model bdm3_UFO
generate p p > chi- chi+ u WEIGHTED<=100
output bdm3_lhc/chiM/chiP/u
launch bdm3_lhc/chiM/chiP/u
analysis=ExRoot
set run_card nevents 10000
set run_card ebeam1 7000
set run_card ebeam2 7000
0
















u,c,t
d,s,b

e,mu,ta
ve,vm,vt

w,a,z,h








=========================QUARKS===========================



	chi+ chi- q
	chi- phiq2+/phiq5+ q 
	chi+ phiq2-/phiq5- q

	phiq2+ phiq2- q
	phiq5+ phiq5- q
	phiq2- phiq5+ q-
	phiq2+ phiq5- q+

	phil- s0/a0 q+
	phil+ s0/a0 q-
	phil+ phil- q

	s0/a0 s0/a0 q


	q+ = u/c/t/d~/s~/b~
	q- = d/s/b/u~/c~/t~







generate pall pall > chi- chi- q
generate pall pall > chi- phiq2- q
generate pall pall > chi- phiq2+ q WEIGHTED<=4
generate pall pall > chi- phiq5- q
generate pall pall > chi- phiq5+ q WEIGHTED<=4
generate pall pall > chi- phil- q
generate pall pall > chi- phil+ q
generate pall pall > chi- s0 q
generate pall pall > chi- a0 q
----------------------------------------------------------
THESE I DIDNT RESUME THE DIAGRAMS
	generate pall pall > chi+ chi- q WEIGHTED<=100
	generate pall pall > chi+ phiq2- q WEIGHTED<=100
	X generate pall pall > chi+ phiq2+ q
	generate pall pall > chi+ phiq5- q WEIGHTED<=100
---------------------------------------------------------
	generate pall pall > chi+ phiq5+ q
	generate pall pall > chi+ phil- q
	generate pall pall > chi+ phil+ q
	generate pall pall > chi+ s0 q
	generate pall pall > chi+ a0 q

generate pall pall > phiq2- phiq2- q
generate pall pall > phiq2- phiq5- q
generate pall pall > phiq2- phiq5+ q- WEIGHTED<=100
generate pall pall > phiq2- phil- q
generate pall pall > phiq2- phil+ q
generate pall pall > phiq2- s0 q
generate pall pall > phiq2- a0 q
	generate pall pall > phiq2+ phiq2- q WEIGHTED<=100
	generate pall pall > phiq2+ phiq5- q+ WEIGHTED<=100
	generate pall pall > phiq2+ phiq5+ q
	generate pall pall > phiq2+ phil- q
	generate pall pall > phiq2+ phil+ q
	generate pall pall > phiq2+ s0 q
	generate pall pall > phiq2+ a0 q

generate pall pall > phiq5- phiq5- q
generate pall pall > phiq5- phil- q
generate pall pall > phiq5- phil+ q
generate pall pall > phiq5- s0 q
generate pall pall > phiq5- a0 q
	generate pall pall > phiq5+ phiq5- q WEIGHTED<=100
	generate pall pall > phiq5+ phil- q
	generate pall pall > phiq5+ phil+ q
	generate pall pall > phiq5+ s0 q
	generate pall pall > phiq5+ a0 q

generate pall pall > phil- phil- q
generate pall pall > phil- s0 q+ WEIGHTED<=100
generate pall pall > phil- a0 q+ WEIGHTED<=100
	generate pall pall > phil+ phil- q WEIGHTED<=100
	generate pall pall > phil+ s0 q- WEIGHTED<=100
	generate pall pall > phil+ a0 q- WEIGHTED<=100

generate pall pall > s0 s0 q WEIGHTED<=100
generate pall pall > a0 a0 q WEIGHTED<=100
generate pall pall > s0 a0 q WEIGHTED<=100








BOSONS GAVE SIMILAR AS QUARKS (only one different), INTERESTING

=========================BOSONS===========================

generate pall pall > chi- chi- a
generate pall pall > chi- phiq2- a
generate pall pall > chi- phiq2+ a/z/h/g/w+ WEIGHTED<=100
generate pall pall > chi- phiq5- a
generate pall pall > chi- phiq5+ a/z/h/g/w- WEIGHTED<=100
generate pall pall > chi- phil- a
generate pall pall > chi- phil+ a
generate pall pall > chi- s0 a
generate pall pall > chi- a0 a
	generate pall pall > chi+ chi- a/z/h/g/w WEIGHTED<=100
	generate pall pall > chi+ phiq2- a/z/h/g/w- WEIGHTED<=100
	generate pall pall > chi+ phiq2+ a
	generate pall pall > chi+ phiq5- a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > chi+ phiq5+ a
	generate pall pall > chi+ phil- a
	generate pall pall > chi+ phil+ a
	generate pall pall > chi+ s0 a
	generate pall pall > chi+ a0 a

generate pall pall > phiq2- phiq2- a
generate pall pall > phiq2- phiq5- a
generate pall pall > phiq2- phiq5+ a/z/h/g/w- WEIGHTED<=100
generate pall pall > phiq2- phil- a
generate pall pall > phiq2- phil+ a
generate pall pall > phiq2- s0 a
generate pall pall > phiq2- a0 a
	generate pall pall > phiq2+ phiq2- a/z/h/g/w WEIGHTED<=100
	generate pall pall > phiq2+ phiq5- a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > phiq2+ phiq5+ a
	generate pall pall > phiq2+ phil- a
	generate pall pall > phiq2+ phil+ a
	generate pall pall > phiq2+ s0 a
	generate pall pall > phiq2+ a0 a

generate pall pall > phiq5- phiq5- a
generate pall pall > phiq5- phil- a
generate pall pall > phiq5- phil+ a
generate pall pall > phiq5- s0 a
generate pall pall > phiq5- a0 a
	generate pall pall > phiq5+ phiq5- a/z/h/g/w WEIGHTED<=100
	generate pall pall > phiq5+ phil- a
	generate pall pall > phiq5+ phil+ a
	generate pall pall > phiq5+ s0 a
	generate pall pall > phiq5+ a0 a

generate pall pall > phil- phil- w+ WEIGHTED<=100
generate pall pall > phil- s0 a/z/h/g/w+ WEIGHTED<=100
generate pall pall > phil- a0 a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > phil+ phil- a/z/h/g/w WEIGHTED<=100
	generate pall pall > phil+ s0 a/z/h/g/w- WEIGHTED<=100
	generate pall pall > phil+ a0 a/z/h/g/w- WEIGHTED<=100

generate pall pall > s0 s0 a/z/h/g/w WEIGHTED<=100
generate pall pall > a0 a0 a/z/h/g/w WEIGHTED<=100
generate pall pall > s0 a0 a/z/h/g/w WEIGHTED<=100








=========================leptons===========================

generate pall pall > chi- chi- l
generate pall pall > chi- phiq2- l
generate pall pall > chi- phiq2+ l
generate pall pall > chi- phiq5- l
generate pall pall > chi- phiq5+ l
generate pall pall > chi- phil- mu+ WEIGHTED<=100
generate pall pall > chi- phil+ mu+/vm~ WEIGHTED<=100
generate pall pall > chi- s0 mu+/vm~ WEIGHTED<=100
generate pall pall > chi- a0 mu+/vm~ WEIGHTED<=100
	generate pall pall > chi+ chi- l
	generate pall pall > chi+ phiq2- l
	generate pall pall > chi+ phiq2+ l
	generate pall pall > chi+ phiq5- l
	generate pall pall > chi+ phiq5+ l
	generate pall pall > chi+ phil- mu-/vm WEIGHTED<=100
	generate pall pall > chi+ phil+ mu- WEIGHTED<=100
	generate pall pall > chi+ s0 mu-/vm WEIGHTED<=100
	generate pall pall > chi+ a0 mu-/vm WEIGHTED<=100

generate pall pall > phiq2- phiq2- l
generate pall pall > phiq2- phiq5- l
generate pall pall > phiq2- phiq5+ l
generate pall pall > phiq2- phil- l
generate pall pall > phiq2- phil+ vm~ WEIGHTED<=100
generate pall pall > phiq2- s0 mu+ WEIGHTED<=100
generate pall pall > phiq2- a0 mu+ WEIGHTED<=100
	generate pall pall > phiq2+ phiq2- l
	generate pall pall > phiq2+ phiq5- l
	generate pall pall > phiq2+ phiq5+ l
	generate pall pall > phiq2+ phil- vm WEIGHTED<=100
	generate pall pall > phiq2+ phil+ l
	generate pall pall > phiq2+ s0 mu- WEIGHTED<=100
	generate pall pall > phiq2+ a0 mu- WEIGHTED<=100

generate pall pall > phiq5- phiq5- l
generate pall pall > phiq5- phil- l
generate pall pall > phiq5- phil+ vm~ WEIGHTED<=100
generate pall pall > phiq5- s0 mu+ WEIGHTED<=100
generate pall pall > phiq5- a0 mu+ WEIGHTED<=100
	generate pall pall > phiq5+ phiq5- l
	generate pall pall > phiq5+ phil- vm WEIGHTED<=100
	generate pall pall > phiq5+ phil+ l
	generate pall pall > phiq5+ s0 mu- WEIGHTED<=100
	generate pall pall > phiq5+ a0 mu- WEIGHTED<=100

generate pall pall > phil- phil- l
generate pall pall > phil- s0 l
generate pall pall > phil- a0 l
	generate pall pall > phil+ phil- l
	generate pall pall > phil+ s0 l
	generate pall pall > phil+ a0 l

generate pall pall > s0 s0 l
generate pall pall > a0 a0 l
generate pall pall > s0 a0 l







generate pall pall > chi- phil- mu+ WEIGHTED<=100
generate pall pall > chi- phil+ mu+/vm~ WEIGHTED<=100
generate pall pall > chi- s0 mu+/vm~ WEIGHTED<=100
generate pall pall > chi- a0 mu+/vm~ WEIGHTED<=100
generate pall pall > chi+ phil- mu-/vm WEIGHTED<=100
generate pall pall > chi+ phil+ mu- WEIGHTED<=100
generate pall pall > chi+ s0 mu-/vm WEIGHTED<=100
generate pall pall > chi+ a0 mu-/vm WEIGHTED<=100

generate pall pall > phiq2- phil+ vm~ WEIGHTED<=100
generate pall pall > phiq2- s0 mu+ WEIGHTED<=100
generate pall pall > phiq2- a0 mu+ WEIGHTED<=100
generate pall pall > phiq2+ phil- vm WEIGHTED<=100
generate pall pall > phiq2+ s0 mu- WEIGHTED<=100
generate pall pall > phiq2+ a0 mu- WEIGHTED<=100

generate pall pall > phiq5- phil+ vm~ WEIGHTED<=100
generate pall pall > phiq5- s0 mu+ WEIGHTED<=100
generate pall pall > phiq5- a0 mu+ WEIGHTED<=100
generate pall pall > phiq5+ phil- vm WEIGHTED<=100
generate pall pall > phiq5+ s0 mu- WEIGHTED<=100
generate pall pall > phiq5+ a0 mu- WEIGHTED<=100






chi phil/s0/a0 mu/vm
phiq2/phiq5 phil vm
phiq2/phiq5 s0/a0 mu




















































=========================QUARKS AND BOSONS===========================

generate pall pall > chi- chi- a
generate pall pall > chi- phiq2- a
generate pall pall > chi- phiq2+ q/a/z/h/g/w+ WEIGHTED<=100
generate pall pall > chi- phiq5- a
generate pall pall > chi- phiq5+ q/a/z/h/g/w- WEIGHTED<=100
generate pall pall > chi- phil- a
generate pall pall > chi- phil+ a
generate pall pall > chi- s0 a
generate pall pall > chi- a0 a
	generate pall pall > chi+ chi- q/a/z/h/g/w WEIGHTED<=100
	generate pall pall > chi+ phiq2- q/a/z/h/g/w- WEIGHTED<=100
	generate pall pall > chi+ phiq2+ a
	generate pall pall > chi+ phiq5- q/a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > chi+ phiq5+ a
	generate pall pall > chi+ phil- a
	generate pall pall > chi+ phil+ a
	generate pall pall > chi+ s0 a
	generate pall pall > chi+ a0 a

generate pall pall > phiq2- phiq2- a
generate pall pall > phiq2- phiq5- a
generate pall pall > phiq2- phiq5+ q-/a/z/h/g/w- WEIGHTED<=100
generate pall pall > phiq2- phil- a
generate pall pall > phiq2- phil+ a
generate pall pall > phiq2- s0 a
generate pall pall > phiq2- a0 a
	generate pall pall > phiq2+ phiq2- q/a/z/h/g/w WEIGHTED<=100
	generate pall pall > phiq2+ phiq5- q+/a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > phiq2+ phiq5+ a
	generate pall pall > phiq2+ phil- a
	generate pall pall > phiq2+ phil+ a
	generate pall pall > phiq2+ s0 a
	generate pall pall > phiq2+ a0 a

generate pall pall > phiq5- phiq5- a
generate pall pall > phiq5- phil- a
generate pall pall > phiq5- phil+ a
generate pall pall > phiq5- s0 a
generate pall pall > phiq5- a0 a
	generate pall pall > phiq5+ phiq5- q/a/z/h/g/w WEIGHTED<=100
	generate pall pall > phiq5+ phil- a
	generate pall pall > phiq5+ phil+ a
	generate pall pall > phiq5+ s0 a
	generate pall pall > phiq5+ a0 a

generate pall pall > phil- phil- w+ WEIGHTED<=100
generate pall pall > phil- s0 q+/a/z/h/g/w+ WEIGHTED<=100
generate pall pall > phil- a0 q+/a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > phil+ phil- q/a/z/h/g/w WEIGHTED<=100
	generate pall pall > phil+ s0 q-/a/z/h/g/w- WEIGHTED<=100
	generate pall pall > phil+ a0 q-/a/z/h/g/w- WEIGHTED<=100

generate pall pall > s0 s0 q/a/z/h/g/w WEIGHTED<=100
generate pall pall > a0 a0 q/a/z/h/g/w WEIGHTED<=100
generate pall pall > s0 a0 q/a/z/h/g/w WEIGHTED<=100







=========================ALL===========================

generate pall pall > chi- chi- a
generate pall pall > chi- phiq2- a
generate pall pall > chi- phiq2+ q/a/z/h/g/w+ WEIGHTED<=100
generate pall pall > chi- phiq5- a
generate pall pall > chi- phiq5+ q/a/z/h/g/w- WEIGHTED<=100
generate pall pall > chi- phil- mu+ WEIGHTED<=100
generate pall pall > chi- phil+ mu+/vm~ WEIGHTED<=100
generate pall pall > chi- s0 mu+/vm~ WEIGHTED<=100
generate pall pall > chi- a0 mu+/vm~ WEIGHTED<=100
	generate pall pall > chi+ chi- q/a/z/h/g/w WEIGHTED<=100
	generate pall pall > chi+ phiq2- q/a/z/h/g/w- WEIGHTED<=100
	generate pall pall > chi+ phiq2+ a
	generate pall pall > chi+ phiq5- q/a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > chi+ phiq5+ a
	generate pall pall > chi+ phil- mu-/vm WEIGHTED<=100
	generate pall pall > chi+ phil+ mu- WEIGHTED<=100
	generate pall pall > chi+ s0 mu-/vm WEIGHTED<=100
	generate pall pall > chi+ a0 mu-/vm WEIGHTED<=100

generate pall pall > phiq2- phiq2- a
generate pall pall > phiq2- phiq5- a
generate pall pall > phiq2- phiq5+ q-/a/z/h/g/w- WEIGHTED<=100
generate pall pall > phiq2- phil- a
generate pall pall > phiq2- phil+ vm~ WEIGHTED<=100
generate pall pall > phiq2- s0 mu+ WEIGHTED<=100
generate pall pall > phiq2- a0 mu+ WEIGHTED<=100
	generate pall pall > phiq2+ phiq2- q/a/z/h/g/w WEIGHTED<=100
	generate pall pall > phiq2+ phiq5- q+/a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > phiq2+ phiq5+ a
	generate pall pall > phiq2+ phil- vm WEIGHTED<=100
	generate pall pall > phiq2+ phil+ g
	generate pall pall > phiq2+ s0 mu- WEIGHTED<=100
	generate pall pall > phiq2+ a0 mu- WEIGHTED<=100

generate pall pall > phiq5- phiq5- a
generate pall pall > phiq5- phil- a
generate pall pall > phiq5- phil+ vm~ WEIGHTED<=100
generate pall pall > phiq5- s0 mu+ WEIGHTED<=100
generate pall pall > phiq5- a0 mu+ WEIGHTED<=100
	generate pall pall > phiq5+ phiq5- q/a/z/h/g/w WEIGHTED<=100
	generate pall pall > phiq5+ phil- vm WEIGHTED<=100
	generate pall pall > phiq5+ phil+ g
	generate pall pall > phiq5+ s0 mu- WEIGHTED<=100
	generate pall pall > phiq5+ a0 mu- WEIGHTED<=100

generate pall pall > phil- phil- w+ WEIGHTED<=100
generate pall pall > phil- s0 q+/a/z/h/g/w+ WEIGHTED<=100
generate pall pall > phil- a0 q+/a/z/h/g/w+ WEIGHTED<=100
	generate pall pall > phil+ phil- q/a/z/h/g/w WEIGHTED<=100
	generate pall pall > phil+ s0 q-/a/z/h/g/w- WEIGHTED<=100
	generate pall pall > phil+ a0 q-/a/z/h/g/w- WEIGHTED<=100

generate pall pall > s0 s0 q/a/z/h/g/w WEIGHTED<=100
generate pall pall > a0 a0 q/a/z/h/g/w WEIGHTED<=100
generate pall pall > s0 a0 q/a/z/h/g/w WEIGHTED<=100





































































=========================g boson===========================

generate pall pall > chi- chi- g WEIGHTED<=100
generate pall pall > chi- phiq2- g WEIGHTED<=100
generate pall pall > chi- phiq2+ g WEIGHTED<=100 
generate pall pall > chi- phiq5- g WEIGHTED<=100
generate pall pall > chi- phiq5+ g WEIGHTED<=100 
generate pall pall > chi- phil- g WEIGHTED<=100
generate pall pall > chi- phil+ g WEIGHTED<=100
generate pall pall > chi- s0 g WEIGHTED<=100
generate pall pall > chi- a0 g WEIGHTED<=100
	generate pall pall > chi+ chi- g WEIGHTED<=100
	generate pall pall > chi+ phiq2- g WEIGHTED<=100
	generate pall pall > chi+ phiq2+ g WEIGHTED<=100
	generate pall pall > chi+ phiq5- g WEIGHTED<=100
	generate pall pall > chi+ phiq5+ g WEIGHTED<=100
	generate pall pall > chi+ phil- g WEIGHTED<=100
	generate pall pall > chi+ phil+ g WEIGHTED<=100
	generate pall pall > chi+ s0 g WEIGHTED<=100
	generate pall pall > chi+ a0 g WEIGHTED<=100

generate pall pall > phiq2- phiq2- g WEIGHTED<=100
generate pall pall > phiq2- phiq5- g WEIGHTED<=100
generate pall pall > phiq2- phiq5+ g WEIGHTED<=100
generate pall pall > phiq2- phil- g WEIGHTED<=100
generate pall pall > phiq2- phil+ g WEIGHTED<=100
generate pall pall > phiq2- s0 g WEIGHTED<=100
generate pall pall > phiq2- a0 g WEIGHTED<=100
	generate pall pall > phiq2+ phiq2- g WEIGHTED<=100
	generate pall pall > phiq2+ phiq5- g WEIGHTED<=100
	generate pall pall > phiq2+ phiq5+ g WEIGHTED<=100
	generate pall pall > phiq2+ phil- g WEIGHTED<=100
	generate pall pall > phiq2+ phil+ g WEIGHTED<=100
	generate pall pall > phiq2+ s0 g WEIGHTED<=100
	generate pall pall > phiq2+ a0 g WEIGHTED<=100

generate pall pall > phiq5- phiq5- g WEIGHTED<=100
generate pall pall > phiq5- phil- g WEIGHTED<=100
generate pall pall > phiq5- phil+ g WEIGHTED<=100
generate pall pall > phiq5- s0 g WEIGHTED<=100
generate pall pall > phiq5- a0 g WEIGHTED<=100
	generate pall pall > phiq5+ phiq5- g WEIGHTED<=100
	generate pall pall > phiq5+ phil- g WEIGHTED<=100
	generate pall pall > phiq5+ phil+ g WEIGHTED<=100
	generate pall pall > phiq5+ s0 g WEIGHTED<=100
	generate pall pall > phiq5+ a0 g WEIGHTED<=100

generate pall pall > phil- phil- g WEIGHTED<=100
generate pall pall > phil- s0 g WEIGHTED<=100
generate pall pall > phil- a0 g WEIGHTED<=100
	generate pall pall > phil+ phil- g WEIGHTED<=100
	generate pall pall > phil+ s0 g WEIGHTED<=100
	generate pall pall > phil+ a0 g WEIGHTED<=100

generate pall pall > s0 s0 g WEIGHTED<=100
generate pall pall > a0 a0 g WEIGHTED<=100
generate pall pall > s0 a0 g WEIGHTED<=100