M$ParametersDark = {





(* external parameters*)

(*direct detection parameter !!!! add reference to xenon paper*)
  lamHS == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 0.01, (*max val = 0.01 from paper*)
    TeX              -> Subscript[\[Lambda], hS ],
    Description      -> "Direct Detection parameter"
  },



(* self couplings external params *)
  lamPQ == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 0.1,
    InteractionOrder -> {NP,1},
    TeX              -> Subscript[\[Lambda], Subscript[ \[CapitalPhi], q ] ],
    Description      -> "PhiQ self-coupling"
  },
  lamPL == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 1.0*^-5,
    InteractionOrder -> {NP,1},
    TeX              -> Subscript[\[Lambda], Subscript[ \[CapitalPhi], l ] ],
    Description      -> "PhiL self-coupling"
  },
            
            
              
              
            
(* Higgs, PhiQ and PhiL interaction couplings *)
  lamHPQ == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 1.0*^-4,
    InteractionOrder -> {QED,2},
    TeX              -> Subscript[ \[Lambda],  H Subscript[ \[CapitalPhi], q ]   ],
    Description      -> "Higgs-PhiQ coupling"
  },
  (*lamHPL == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 1.01891,
    InteractionOrder -> {QED,2},
    TeX              -> Subscript[ \[Lambda],  H Subscript[ \[CapitalPhi], l ]   ],
    Description      -> "Higgs-PhiL coupling"
  },*)
  lamPQPL == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 1.0*^-3,
    InteractionOrder -> {NP,1},
    TeX              -> Subscript[ \[Lambda],  Subscript[ \[CapitalPhi], q ]  Subscript[ \[CapitalPhi], l ]   ],
    Description      -> "PhiQ-PhiL coupling"
  },
  lamPQPLprime == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 1.0*^-3,
    InteractionOrder -> {NP,1},
    TeX              -> Subscript[ Superscript[ \[Lambda], \[Prime] ] ,  Subscript[ \[CapitalPhi], q ]  Subscript[ \[CapitalPhi], l ]   ],
    Description      -> "PhiQ-PhiL coupling 2"
  },
  yHPQ == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 1.0*^-3,
    InteractionOrder -> {QED,2},
    TeX              -> Subscript[ y , H Subscript[ \[CapitalPhi], q ] ],
    Description      -> "H-PhiL sigma coupling"
  },
  yPQPL == {
    ParameterType    -> External,
    BlockName        -> DARKCOUPLINGS,
    Value            -> 1.0*^-3,
    InteractionOrder -> {NP,1},
    TeX              -> Subscript[ y ,  Subscript[ \[CapitalPhi], q ]  Subscript[ \[CapitalPhi], l ]   ],
    Description      -> "PhiQ-PhiL sigma coupling"
  },
        
        
        
        
        
        
        
(* internal parameters *)
  
  muPQ == { 
    ParameterType    -> Internal, 
    Value            -> Sqrt[ MphiQ5*MphiQ5 - (lamHPQ+yHPQ)*vev*vev/2 ],
    InteractionOrder -> {NP,1}, 
    TeX              -> Subscript[ \[Mu] , Subscript[ \[CapitalPhi], q ] ],
    Description      -> "PhiQ prior to EWB mass term"  (*!!!!fix these descritpions*)
  },
  muPL == { 
    ParameterType    -> Internal, 
    Value            -> Sqrt[ ( 2*MphiLp*MphiLp - lamHPL*vev*vev )/2 ],
    InteractionOrder -> {NP,1}, 
    TeX              -> Subscript[ \[Mu] , Subscript[ \[CapitalPhi], l ] ],
    Description      -> "PhiL prior to EWB mass term"
  },


lamHPL == {
    ParameterType    -> Internal,
    Value            -> lamHS + 2*( MphiLp*MphiLp - MS0*MS0 )/(vev*vev),
    InteractionOrder -> {QED,2},
    TeX              -> Subscript[ \[Lambda],  H Subscript[ \[CapitalPhi], l ]   ],
    Description      -> "Higgs-PhiL coupling"
  },
  lamHPLprime == { 
    ParameterType    -> Internal, 
    Value            -> ( MS0*MS0 + MA0*MA0 - 2* MphiLp*MphiLp )/(vev*vev),
    InteractionOrder -> {QED,2}, 
    TeX              -> Subscript[ Superscript[ \[Lambda], \[Prime] ] , H Subscript[ \[CapitalPhi], l ] ],
    Description      -> "..."
  },
  lamHPQprime == { 
    ParameterType    -> Internal, 
    Value            -> ( 2*MphiQ2*MphiQ2 - 2*MphiQ5*MphiQ5 + vev*vev*yHPQ )/(vev*vev),
    InteractionOrder -> {QED,2}, 
    TeX              -> Subscript[ Superscript[ \[Lambda], \[Prime] ] , H Subscript[ \[CapitalPhi], q ] ],
    Description      -> "..."
  },


  lam5 == { 
    ParameterType    -> Internal, 
    Value            -> ( MS0*MS0 - MA0*MA0 )/(2*vev*vev),
    InteractionOrder -> {QED,2}, 
    TeX              -> Subscript[ \[Lambda] , 5 ],
    Description      -> "..."
  },
  
  




  


  ymsD == {
    ParameterType -> External,
    BlockName     -> YUKAWADARK,
    Value         -> -0.1,
    InteractionOrder -> {NP,1},
    Description   -> "Strange Yukawa Dark coupling"
  },
  ymbD == {
    ParameterType -> External,
    BlockName     -> YUKAWADARK,
    Value         -> 0.4,
    InteractionOrder -> {NP,1},
    Description   -> "Bottom Yukawa Dark coupling"
  },
  ymmD == {
    ParameterType -> External,
    BlockName     -> YUKAWADARK,
    Value         -> 4,
    InteractionOrder -> {NP,1},
    Description   -> "Muon Yukawa Dark coupling"
  },

  
  
  ydD == {
    ParameterType    -> Internal,
    Indices          -> {Index[Generation]},
    Definitions      -> {ydD[1] :> 0, ydD[2] :> ymsD, ydD[3] :> ymbD},
(*    Value            -> {ydD[1] -> 0, ydD[2] -> ymsD, ydD[3] -> ymbD},*)
    InteractionOrder -> {NP,1},
    ParameterName    -> {ydD[1] -> ydoD, ydD[2] -> ysD, ydD[3] -> ybD},
    TeX              -> ydD,
    Description      -> "Down-type Yukawa Dark couplings"
  },
  ylD == {
    ParameterType    -> Internal,
    Indices          -> {Index[Generation]},
    Definitions      -> { ylD[1] :> 0, ylD[2] :> ymmD, ylD[3] :> 0 },
(*    Value            -> {ylD[1] -> 0, ylD[2] -> ymmD, ylD[3] -> 0},*)
    InteractionOrder -> {NP,1},
    ParameterName    -> {ylD[1] -> yeD, ydD[2] -> ymD, ydD[3] -> ytauD},
    TeX              -> ylD,
    Description      -> "Lepton Yukawa Dark couplings"
  },








  pauliSigma2 == { 
    ParameterType -> Internal,
    Indices       -> {Index[SU2D], Index[SU2D]},
    Unitary       -> True,
    ComplexParameter -> False, (*due to what seems to be a bug this needs to be defined here otherway decays.py in madgraph will give problems as it does no have defined complexconjugate*)
    Definitions         -> {pauliSigma2[1,1] :> 0,  pauliSigma2[1,2] :> -I,
                      pauliSigma2[2,1] :> I,  pauliSigma2[2,2] :> 0 },
    TeX         -> Subscript[ \[Sigma] , 2 ],
    Description -> "Second Pauli Sigma matrix"}
    
    



  
};


















