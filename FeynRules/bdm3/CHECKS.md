- [ ] Feynrules checks like masses, hermicity and the kinetic normalization
- [ ] Maybe compare mathematica terms to the one exiting from feynrules
        This will ensure I wrote things correctly in feynrules
        In mathemaica i understand very well whats happening and it's basically the same as writting it out by hand
- [ ] Compare the lagrangian I wrote better with the one by rodrigo
- [x] Quick look at the diagrams see if they are similar to model5 paper
- [ ] Check diagrams from paper model 3
