(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11497,        366]
NotebookOptionsPosition[     10238,        339]
NotebookOutlinePosition[     10635,        355]
CellTagsIndexPosition[     10592,        352]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell["\<\
vl   = {ve, vm,  vt}
l     = {e, mu, ta}
uq = {u,  c,  t}
dq = {d, s, b}\
\>", "Text",
 CellChangeTimes->{{3.8496429575248632`*^9, 
  3.849643060809565*^9}},ExpressionUUID->"2f8c340b-088d-4cce-8f58-\
509b038df4fa"],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["L", "L"], "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"(", GridBox[{
         {
          SubscriptBox["ve", "L"]},
         {
          SubscriptBox["e", "L"]}
        }], ")"}], ",", 
      RowBox[{"(", GridBox[{
         {
          SubscriptBox["vm", "L"]},
         {
          SubscriptBox["mu", "L"]}
        }], ")"}], ",", 
      RowBox[{"(", GridBox[{
         {
          SubscriptBox["vt", "L"]},
         {
          SubscriptBox["tau", "L"]}
        }], ")"}]}], "}"}]}], TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "82a8a166-17ce-435f-ab60-c7239d62a4e6"],
 "\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["l", "R"], "=", 
    RowBox[{"{", 
     RowBox[{
      SubscriptBox["e", "R"], ",", 
      SubscriptBox["mu", "R"], ",", 
      SubscriptBox["tau", "R"]}], "}"}]}], TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "8c882bab-9a58-4b55-bc81-f3f27ee61136"]
}], "Text",
 CellChangeTimes->{{3.8496432134462757`*^9, 3.8496432143315973`*^9}, {
  3.849643249547441*^9, 3.849643256238347*^9}, {3.849643286831215*^9, 
  3.849643345672236*^9}, {3.8496434485220547`*^9, 3.849643497350811*^9}, {
  3.84964365033871*^9, 
  3.8496436544173307`*^9}},ExpressionUUID->"41f349e4-f214-4c15-8a43-\
f7f08f7bc739"],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["V", "CKM"], "=", 
    RowBox[{"(", GridBox[{
       {
        SubscriptBox["c", "\[Theta]c"], 
        SubscriptBox["s", "\[Theta]c"], "0"},
       {
        RowBox[{"-", 
         SubscriptBox["s", "\[Theta]c"]}], 
        SubscriptBox["c", "\[Theta]c"], "0"},
       {"0", "0", "1"}
      }], ")"}]}], TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "075fd711-6ddd-455e-8c72-be5a4c0d580a"],
 "\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["V", "CKM"], " ", "dq"}], "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{
        SubscriptBox["c", "\[Theta]c"], 
        SubscriptBox["d", "L"]}], "+", 
       RowBox[{
        SubscriptBox["s", "\[Theta]c"], 
        SubscriptBox["s", "L"]}]}], ",", 
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         SubscriptBox["s", "\[Theta]c"]}], 
        SubscriptBox["d", "L"]}], "+", 
       RowBox[{
        SubscriptBox["c", "\[Theta]c"], 
        SubscriptBox["s", "L"]}]}], ",", 
      SubscriptBox["b", "L"]}], "}"}]}], TraditionalForm]],ExpressionUUID->
  "c760e6a8-f3fa-4764-af4c-7e68af59efae"]
}], "Text",
 CellChangeTimes->{{3.8496447335196667`*^9, 3.8496448010046988`*^9}, {
  3.849644851851624*^9, 3.84964487767243*^9}, {3.849644946740382*^9, 
  3.8496449467405252`*^9}, {3.849645048364544*^9, 3.849645049203472*^9}, {
  3.8496464777641*^9, 
  3.849646535280796*^9}},ExpressionUUID->"9bba1821-fa41-4055-a554-\
383b779f199b"],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["Q", "L"], "=", 
    RowBox[{"(", GridBox[{
       {"uq"},
       {
        RowBox[{
         SubscriptBox["V", "CKM"], " ", "dq"}]}
      }], ")"}]}], TraditionalForm]],ExpressionUUID->
  "d9569081-2ee3-4f17-bd69-4f23f24a8000"],
 "\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["Q", "L"], "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"(", GridBox[{
         {
          SubscriptBox["u", "L"]},
         {
          RowBox[{
           RowBox[{
            SubscriptBox["c", "\[Theta]c"], 
            SubscriptBox["d", "L"]}], "+", 
           RowBox[{
            SubscriptBox["s", "\[Theta]c"], 
            SubscriptBox["s", "L"]}]}]}
        }], ")"}], ",", 
      RowBox[{"(", GridBox[{
         {
          SubscriptBox["c", "L"]},
         {
          RowBox[{
           RowBox[{
            RowBox[{"-", 
             SubscriptBox["s", "\[Theta]c"]}], 
            SubscriptBox["d", "L"]}], "+", 
           RowBox[{
            SubscriptBox["c", "\[Theta]c"], 
            SubscriptBox["s", "L"]}]}]}
        }], ")"}], ",", 
      RowBox[{"(", GridBox[{
         {
          SubscriptBox["t", "L"]},
         {
          SubscriptBox["b", "L"]}
        }], ")"}]}], "}"}]}], TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "ed38677d-f3ca-438a-9b77-44d5a03d88c2"]
}], "Text",
 CellChangeTimes->{{3.849644916052706*^9, 3.849645057372644*^9}, {
  3.8496451102984858`*^9, 3.84964514885084*^9}, {3.849646468706851*^9, 
  3.8496464695897713`*^9}, {3.849646542976754*^9, 
  3.849646561029491*^9}},ExpressionUUID->"aa33c4a0-839e-4e32-8d0a-\
39555359f569"],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["u", "R"], "=", 
    RowBox[{"{", 
     RowBox[{
      SubscriptBox["u", "R"], ",", 
      SubscriptBox["c", "R"], ",", 
      SubscriptBox["t", "R"]}], "}"}]}], TraditionalForm]],ExpressionUUID->
  "ae544236-48f2-476e-83c0-1f62ce6bda70"],
 "\n",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["d", "R"], "=", 
    RowBox[{"{", 
     RowBox[{
      SubscriptBox["d", "R"], ",", 
      SubscriptBox["s", "R"], ",", 
      SubscriptBox["b", "R"]}], "}"}]}], TraditionalForm]],ExpressionUUID->
  "009b117c-2809-41a9-b235-9d647f017140"]
}], "Text",
 CellChangeTimes->{{3.849643692792519*^9, 3.849643740284606*^9}, {
  3.84964378719508*^9, 3.849643793768284*^9}, {3.849644270707592*^9, 
  3.849644297579268*^9}, {3.849644891101494*^9, 
  3.8496449233856907`*^9}},ExpressionUUID->"d7b82830-c2e1-4a87-9f90-\
762cba2feac0"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"s12", "=", "0.2229"}], ";", 
  RowBox[{"s23", "=", "0.0412"}], ";", 
  RowBox[{"s13", "=", "0.0036"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"c12", "=", 
   RowBox[{"Sqrt", "[", 
    RowBox[{"1", "-", 
     SuperscriptBox["s12", "2"]}], "]"}]}], ";", 
  RowBox[{"c23", "=", 
   RowBox[{"Sqrt", "[", 
    RowBox[{"1", "-", 
     SuperscriptBox["s23", "2"]}], "]"}]}], ";", 
  RowBox[{"c13", "=", 
   RowBox[{"Sqrt", "[", 
    RowBox[{"1", "-", 
     SuperscriptBox["s13", "2"]}], "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.8497064285867147`*^9, 3.849706439942625*^9}, {
  3.849706587078772*^9, 3.849706614835387*^9}},
 CellLabel->"In[2]:=",ExpressionUUID->"5ad42858-2de7-43f4-b37b-d9ec4720e33a"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", GridBox[{
     {
      RowBox[{"c12", " ", "c13"}], 
      RowBox[{"s12", " ", "c13"}], "s13"},
     {
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"-", "s12"}], " ", "c23"}], "-", 
        RowBox[{"c12", " ", "s23", " ", "s13"}]}], ")"}], 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"c12", " ", "c23"}], "-", 
        RowBox[{"s12", " ", "s23", " ", "s13"}]}], ")"}], 
      RowBox[{"s23", " ", "c13"}]},
     {
      RowBox[{"(", 
       RowBox[{
        RowBox[{"s12", " ", "s23"}], "-", 
        RowBox[{"c12", " ", "c23", " ", "s13"}]}], ")"}], 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"-", "c12"}], " ", "s23"}], "-", 
        RowBox[{"s12", " ", "c23", " ", "s13"}]}], ")"}], 
      RowBox[{"c23", " ", "c13"}]}
    }], ")"}], "//", "TableForm"}]], "Input",
 CellChangeTimes->{{3.849706446255053*^9, 3.8497065813463163`*^9}, {
  3.849706619279951*^9, 3.8497066203992043`*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"e52ebb34-b99f-4140-a17c-efd283988e48"],

Cell[BoxData[
 TagBox[GridBox[{
    {"0.9748349982996884`", "0.22289855560332011`", "0.0036`"},
    {
     RowBox[{"-", "0.22285532842738381`"}], "0.9739805360428643`", 
     "0.04119973302313499`"},
    {"0.005677031052344888`", 
     RowBox[{"-", "0.040965220853894`"}], "0.9991444450122426`"}
   },
   GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
   GridBoxSpacings->{"Columns" -> {
       Offset[0.27999999999999997`], {
        Offset[2.0999999999999996`]}, 
       Offset[0.27999999999999997`]}, "Rows" -> {
       Offset[0.2], {
        Offset[0.4]}, 
       Offset[0.2]}}],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.849706616719371*^9, 3.849706620781466*^9}},
 CellLabel->
  "Out[5]//TableForm=",ExpressionUUID->"57ad5f6a-0b22-4ca8-ab92-082b4912f522"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", GridBox[{
      {
       RowBox[{"Cos", "[", "\[Theta]", "]"}], 
       RowBox[{"Sin", "[", "\[Theta]", "]"}], "0"},
      {
       RowBox[{"-", 
        RowBox[{"Sin", "[", "\[Theta]", "]"}]}], 
       RowBox[{"Cos", "[", "\[Theta]", "]"}], "0"},
      {"0", "0", "1"}
     }], ")"}], "/.", 
   RowBox[{"(", 
    RowBox[{"\[Theta]", "\[Rule]", "0.227736"}], ")"}]}], "//", 
  "TableForm"}]], "Input",
 CellChangeTimes->{{3.849706659424255*^9, 3.849706719353381*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"1a40ebab-8b3f-4e9d-86d4-967c95b928ed"],

Cell[BoxData[
 TagBox[GridBox[{
    {"0.974180040319821`", "0.2257725604285693`", "0"},
    {
     RowBox[{"-", "0.2257725604285693`"}], "0.974180040319821`", "0"},
    {"0", "0", "1"}
   },
   GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
   GridBoxSpacings->{"Columns" -> {
       Offset[0.27999999999999997`], {
        Offset[2.0999999999999996`]}, 
       Offset[0.27999999999999997`]}, "Rows" -> {
       Offset[0.2], {
        Offset[0.4]}, 
       Offset[0.2]}}],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.849706719620884*^9},
 CellLabel->
  "Out[6]//TableForm=",ExpressionUUID->"ba1cc92c-daaa-48dc-9aff-627728cb7e26"]
}, Open  ]]
},
WindowSize->{606, 483},
WindowMargins->{{Automatic, 63.75}, {12, Automatic}},
FrontEndVersion->"12.2 for Linux x86 (64-bit) (December 12, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"d6cd3bb9-aa96-4c66-8f42-69e5e2dba933"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 224, 8, 104, "Text",ExpressionUUID->"2f8c340b-088d-4cce-8f58-509b038df4fa"],
Cell[785, 30, 1347, 45, 70, "Text",ExpressionUUID->"41f349e4-f214-4c15-8a43-f7f08f7bc739"],
Cell[2135, 77, 1523, 48, 90, "Text",ExpressionUUID->"9bba1821-fa41-4055-a554-383b779f199b"],
Cell[3661, 127, 1671, 57, 89, "Text",ExpressionUUID->"aa33c4a0-839e-4e32-8d0a-39555359f569"],
Cell[5335, 186, 894, 27, 54, "Text",ExpressionUUID->"d7b82830-c2e1-4a87-9f90-762cba2feac0"],
Cell[6232, 215, 749, 20, 98, "Input",ExpressionUUID->"5ad42858-2de7-43f4-b37b-d9ec4720e33a"],
Cell[CellGroupData[{
Cell[7006, 239, 1064, 31, 85, "Input",ExpressionUUID->"e52ebb34-b99f-4140-a17c-efd283988e48"],
Cell[8073, 272, 824, 21, 79, "Output",ExpressionUUID->"57ad5f6a-0b22-4ca8-ab92-082b4912f522"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8934, 298, 596, 17, 63, "Input",ExpressionUUID->"1a40ebab-8b3f-4e9d-86d4-967c95b928ed"],
Cell[9533, 317, 689, 19, 128, "Output",ExpressionUUID->"ba1cc92c-daaa-48dc-9aff-627728cb7e26"]
}, Open  ]]
}
]
*)

