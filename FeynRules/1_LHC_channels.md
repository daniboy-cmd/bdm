# Model 3

- [ ] phiq2+ phiq2- u 
    Seems interesting as there is order 3 QCD with 6 diagrams and by increasing this you get a lot more diagrams from QCD
    But the total order is the same aka 3*QCD = 1*QCD+2*QED which means the 3 qcd should be more likely (check by generating)
- [ ] phiq2+ phiq2- b
    Maybe b tagging can help this process, especially since phiq2+ mostly decays to b quarks
    there is some background where you can replace one of the phiq2- for another dark particle i think
- [ ] same as previouses but with phiq5+
    phiq5+ decays mostly with top but that is interesting as well since it decays quickly and it doesnt decay to the new particles luckily
- [ ] p p > allDark allDark b
    A lot of diagrams so maybe write them down by generating something
    also this is good to find backgrounds
- [ ] p p > s0 s0 w-
    Gave a pretty high cross=0.06    seems quite high but what do i know
    
- [ ] IMPORTANT: limited to double Dark particles
    I guess unless I consider only Dark intermediates but that seems to many orders of magnitude too small. MAybe check this with a calc
- [ ] Maybe take advantage of the loop diagram somehow as almost all diagrams seem to be a variation of this one. Like consider all the possible cuts.
    Maybe a good justification why this is a good idea to do is because we already know the loop should be significant. So also consider the loop with only b and s maybe.
- [ ] Seems like what you do with phiq2 is the same as phiq5 but just exchange an u with a s    


lOOk AT sTANdRD HIGGS DOUBLET FEYNMAN RULES to better understand phil and the others
