(******************************************************************)
(*     Restriction file for 2HDM.fr                               *)
(*                                                                *)                                            
(*     CP symmetry                                                *)
(******************************************************************)

M$Restrictions = {
       
   (* G(L,D,U) have to be hermitian, together with the flavor symmetry, this reduce to  : *)      
          GLI[i_, i_] :> 0,
          GDI[i_, i_] :> 0,
          GUI[i_, i_] :> 0
}
