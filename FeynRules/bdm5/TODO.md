
# ToDo

- [ ] Do I need to actually use chiR or just chi as i've done it? in other words do i need to use PR in the yukawa terms?
- [ ] Change author and redits information
- [x] For some reason for PhiQ when I use 	QuantumNumbers -> { Q -> 2/3 } instead of Y -> 2/3 its missing the photon vertices
		Ok I think it's because the SM model I used didn't define Q so basically it's just a number that feynrules checks is conserved. It's not actually used to figure out the Y automatically. In other words I have to give both Y and Q. For the other fields this didn't happen since It gt the Q from the physical fields (chiM,chi0) and the Y from the doublet (chi).
		Other problem is that when I do this feynrules thinks the Y is not conserved as the Y is not passed down from fields (like the doublet) to the physical fields (the components of the field). So to solve this I have 3 options: Define Q gauge group so i can take out Y from PhiQ, define an unphysical PhiQ that has only the Y and the physical with Q (but theyre the same) or try to give the other fields Y but this runs into the problem of what to give quarks since they're not left (y=1/2) nor right (y=1/6).
		Solved it by just making a new unphysical field and physical field distinction even though it's the same
- [ ] For the same reason as previous the Lepton Number is a problem but I can't really fix it easily, only way is to remove the Lepton numbers from the leptons
- [ ] Confirmar como deve ser o vertice G phiq Phiq
- [x] Os sinais do LkinChi nao deu igual aos outros. Verificar com os SM vertices qual esta certo
- [ ] Coupling order add NP
- [x] complexconjugate error
		In decays.py it was using that function but not importing it. 
		FIXED by defining CKM matrix as real since those were the conjugates appearing.
- [ ] confirmar os sinais dos termos do lagrangiano
- [ ] add proper block for mchi
- [ ] see if taking [col1] from phiQ DC will fix the cross section
