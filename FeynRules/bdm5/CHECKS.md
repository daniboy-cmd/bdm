

# ToDo

- [x] comparing terms (that I wroe in the feynrules files) with the bdm implementation and the Leptoquark model

        **LeptoQ** : I ChiPbar.Ga[mu].DC[ChiP,mu]                       - Mchi1 ChiPbar.ChiP 
        **bdm**    :       Chi*gamma*(i*deriv-g*taupm*WW/2+g1*B1/2)*chi - Mchi*Chi*chi.
        **mine**   : I*(Chibar.Ga[mu].DC[Chi, mu])                      - Mchi Chibar[sp1,ii].ProjP[sp1,sp2].Chi[sp2, ii]- HC[ Mchi ... ]

        **LeptoQ** : DC[S1bar[cc],mu] DC[S1[cc],mu]
        **bdm**    : ( (deriv^mu-i*2/3*g1*B1^mu)*'~Pq'^a + i*GG*'~Pq'^b*lambda^b^a^c*G^mu^c ) * ( (deriv^mu+i*2/3*g1*B1^mu)*'~pq'^a - i*GG*lambda^a^b^c*G^mu^c*'~pq'^b ) = deriv^mu*'~Pq'^a * deriv^mu*'~pq'^a  + ...
        **mine**   : DC[PhiQbar[cc1],mu] DC[PhiQ[cc1], mu]
        
        **bdm**    : (deriv^mu)*Pl*(deriv^mu)*pl.
        **mine**   : DC[PhiLbar,mu] DC[PhiL, mu]
        
        **bdm**    :  yb*Q3b*(1+g5)/2*chi*'~pq' + ys*Q2b*(1+g5)/2*chi*'~pq' + ym*L2*(1+g5)/2*chi*pl 
                     +yb*Chi*(1-g5)/2*q3b*'~Pq' + ys*Chi*(1-g5)/2*q2b*'~Pq' + ym*Chi*(1-g5)/2*l2*Pl.
        **mine**   :       ydD[ff1] CKM[ff2, ff1] QLbar[sp1, ii, ff2, cc].ProjP[sp1,sp2].Chi[sp2, ii]  PhiQ[cc]
                          +ylD[ff1]               LLbar[sp1, ii, ff1]    .ProjP[sp1,sp2].Chi[sp2, ii]  PhiL
                          +HC[]
        
        **bdm**    : muH2*PP*pp                 -lamH*(pp*PP)**2
        **mine**   : muH^2 Phibar[ii] Phi[ii]   -lamH  Phibar[ii] Phi[ii] Phibar[jj] Phi[jj]
        
        **bdm**    : -muPq2*'~Pq'*'~pq'               -lamPq*('~Pq'*'~pq')*('~Pq'*'~pq') 
        **mine**   : -muPQ^2 PhiQbar[cc1] PhiQ[cc1]   -lamPQ   PhiQbar[cc1] PhiQ[cc1] PhiQbar[cc2] PhiQ[cc2], 
        
        **bdm**    : -muPl2*Pl*pl           -lamPl*(Pl*pl)**2
        **mine**   : -muPL^2 PhiLbar PhiL   -lamPL PhiLbar PhiL PhiLbar PhiL 
        **bdm**    : -muPlp2*(Pl*Pl+pl*pl)/2                             -lamPlp*(Pl*Pl+pl*pl)**2/4
        **mine**   : -muPLprime^2 / 2  ( PhiL PhiL + PhiLbar PhiLbar )   -lamPLprime/4  ( PhiL PhiL + PhiLbar PhiLbar )^2,
        
        **bdm**    : -kapHPq*(PP*pp)*('~Pq'*'~pq')                      -kapHPl*(PP*pp)*(Pl*pl)
        **mine**   : -lamHPQ (Phibar[ii] Phi[ii])(PhiQbar[cc] PhiQ[cc]) -lamHPL (Phibar[ii] Phi[ii])(PhiLbar PhiL)
        **bdm**    : -kapPqPl*('~Pq'*'~pq')*(Pl*pl)
        **mine**   : -lamPQPL (PhiQbar[cc] PhiQ[cc])(PhiLbar PhiL)      
        **bdm**    : -kapHPlp*(PP*pp)*(Pl*Pl+pl*pl)/2                               -kapPqPlp*('~Pq'*'~pq')*(Pl*Pl+pl*pl)/2 
        **mine**   : -lamHPLprime/2 (Phibar[ii] Phi[ii])(PhiL PhiL+PhiLbar PhiLbar) -lamPQPLprime/2 (PhiQbar[cc] PhiQ[cc])(PhiL PhiL+PhiLbar PhiLbar)
        
- [x] Checked feynam rules from the models output (lanhep with this). Some signs weren't the same but this difference was also present in SM terms
- [~] Cross sectios with model 5 article
    Worked out but one of them was 2x the supposed
        
        
        
        
        
