import model bdm3_UFO
generate p p > chi- chi+ u WEIGHTED<=100
output bdm3_lhc/chiM/chiP/u
launch bdm3_lhc/chiM/chiP/u
analysis=ExRoot
set run_card nevents 10000
set run_card ebeam1 7000
set run_card ebeam2 7000
0
















u,c,t
d,s,b

e,mu,ta
ve,vm,vt

w,a,z,h








=========================LEPTONS===========================


chi A0/S0 mu/vm
phiQ A0/S0 mu/vm


generate p p > chi- chi- lvl
generate p p > chi- chi+ lvl
generate p p > chi- chi0 lvl
generate p p > chi- CHI0 lvl
generate p p > chi- phiQ lvl
generate p p > chi- PHIQ lvl
generate p p > chi- A0 mu+/vm~ WEIGHTED<=100
generate p p > chi- S0 mu+/vm~ WEIGHTED<=100
	generate p p > chi+ chi+ lvl
	generate p p > chi+ chi0 lvl
	generate p p > chi+ CHI0 lvl
	generate p p > chi+ phiQ lvl
	generate p p > chi+ PHIQ lvl
	generate p p > chi+ A0 mu-/vm WEIGHTED<=100
	generate p p > chi+ S0 mu-/vm WEIGHTED<=100

generate p p > chi0 chi0 lvl
generate p p > chi0 CHI0 lvl
generate p p > chi0 phiQ lvl
generate p p > chi0 PHIQ lvl
generate p p > chi0 A0 mu+/vm~ WEIGHTED<=100
generate p p > chi0 S0 mu+/vm~ WEIGHTED<=100
	generate p p > CHI0 CHI0 lvl
	generate p p > CHI0 phiQ lvl
	generate p p > CHI0 PHIQ lvl
	generate p p > CHI0 A0 mu-/vm WEIGHTED<=100
	generate p p > CHI0 S0 mu-/vm WEIGHTED<=100

generate p p > phiQ phiQ lvl
generate p p > phiQ PHIQ lvl
generate p p > phiQ A0 mu-/vm WEIGHTED<=100
generate p p > phiQ S0 mu-/vm WEIGHTED<=100
	generate p p > PHIQ PHIQ lvl
	generate p p > PHIQ A0 mu+/vm~ WEIGHTED<=100
	generate p p > PHIQ S0 mu+/vm~ WEIGHTED<=100

generate p p > A0 A0 lvl
generate p p > A0 S0 lvl

generate p p > S0 S0 lvl






=========================QUARKS===========================



chi/phiQ chi/phiQ q
A0 A0 q
S0 S0 q






generate p p > chi- chi- q
generate p p > chi- chi+ q WEIGHTED<=100 top doesn't work
generate p p > chi- chi0 q
generate p p > chi- CHI0 q+ WEIGHTED<=100
generate p p > chi- phiQ q WEIGHTED<=100
generate p p > chi- PHIQ q
generate p p > chi- A0 q
generate p p > chi- S0 q
	generate p p > chi+ chi+ q
	generate p p > chi+ chi0 q- WEIGHTED<=100
	generate p p > chi+ CHI0 q
	generate p p > chi+ phiQ q
	generate p p > chi+ PHIQ q WEIGHTED<=100
	generate p p > chi+ A0 q
	generate p p > chi+ S0 q

generate p p > chi0 chi0 q
generate p p > chi0 CHI0 q WEIGHTED<=100   q'=all except bottom  (antibottom works) but pall works for all
generate p p > chi0 phiQ q WEIGHTED<=100
generate p p > chi0 PHIQ q
generate p p > chi0 A0 q
generate p p > chi0 S0 q
	generate p p > CHI0 CHI0 q
	generate p p > CHI0 phiQ q
	generate p p > CHI0 PHIQ q WEIGHTED<=100
	generate p p > CHI0 A0 q
	generate p p > CHI0 S0 q

generate p p > phiQ phiQ q
generate p p > phiQ PHIQ q WEIGHTED<=100
generate p p > phiQ A0 q
generate p p > phiQ S0 q
	generate p p > PHIQ PHIQ q
	generate p p > PHIQ A0 q
	generate p p > PHIQ S0 q

generate p p > A0 A0 q' WEIGHTED<=100  q'=all except bottom  (antibottom works) but pall works 
generate p p > A0 S0 q

generate p p > S0 S0 q WEIGHTED<=100











generate p p > chi- chi+ q WEIGHTED<=100
generate p p > chi- CHI0 q+ WEIGHTED<=100
generate p p > chi- phiQ q WEIGHTED<=100
generate p p > chi+ chi0 q- WEIGHTED<=100
generate p p > chi+ PHIQ q WEIGHTED<=100
generate p p > chi0 CHI0 q' WEIGHTED<=100
generate p p > chi0 phiQ q WEIGHTED<=100
generate p p > CHI0 PHIQ q WEIGHTED<=100
generate p p > phiQ PHIQ q WEIGHTED<=100
generate p p > A0 A0 q WEIGHTED<=100
generate p p > S0 S0 q WEIGHTED<=100


=========================BOSONS===========================



generate pall pall > chi- chi+ V WEIGHTED<=100
generate pall pall > chi- CHI0 V (no w-) WEIGHTED<=100
generate pall pall > chi- phiQ V (no w-) WEIGHTED<=100
generate pall pall > chi+ chi0 V (no w+) WEIGHTED<=100
generate pall pall > chi+ PHIQ V (no w+) WEIGHTED<=100
generate pall pall > chi0 CHI0 V WEIGHTED<=100
generate pall pall > chi0 phiQ V (no w+) WEIGHTED<=100
generate pall pall > CHI0 PHIQ V (no w-) WEIGHTED<=100
generate pall pall > phiQ PHIQ V WEIGHTED<=100
generate pall pall > A0 A0 V WEIGHTED<=100
generate pall pall > S0 S0 V WEIGHTED<=100





generate pall pall > chi- chi- V
generate pall pall > chi- chi+ V WEIGHTED<=100
generate pall pall > chi- chi0 V
generate pall pall > chi- CHI0 V (no w-) WEIGHTED<=100
generate pall pall > chi- phiQ V (no w-) WEIGHTED<=100
generate pall pall > chi- PHIQ V
generate pall pall > chi- A0 V
generate pall pall > chi- S0 V
	generate pall pall > chi+ chi+ V
	generate pall pall > chi+ chi0 V (no w+) WEIGHTED<=100
	generate pall pall > chi+ CHI0 V
	generate pall pall > chi+ phiQ V
	generate pall pall > chi+ PHIQ V (no w+) WEIGHTED<=100
	generate pall pall > chi+ A0 V
	generate pall pall > chi+ S0 V

generate pall pall > chi0 chi0 V
generate pall pall > chi0 CHI0 V WEIGHTED<=100
generate pall pall > chi0 phiQ V (no w+) WEIGHTED<=100
generate pall pall > chi0 PHIQ V
generate pall pall > chi0 A0 V
generate pall pall > chi0 S0 V
	generate pall pall > CHI0 CHI0 V
	generate pall pall > CHI0 phiQ V
	generate pall pall > CHI0 PHIQ V (no w-) WEIGHTED<=100
	generate pall pall > CHI0 A0 V
	generate pall pall > CHI0 S0 V

generate pall pall > phiQ phiQ V
generate pall pall > phiQ PHIQ V WEIGHTED<=100
generate pall pall > phiQ A0 V
generate pall pall > phiQ S0 V
	generate pall pall > PHIQ PHIQ V
	generate pall pall > PHIQ A0 V
	generate pall pall > PHIQ S0 V

generate pall pall > A0 A0 V WEIGHTED<=100
generate pall pall > A0 S0 V

generate pall pall > S0 S0 V WEIGHTED<=100

















































































generate p p > chi- chi- l WEIGHTED<=100
generate p p > chi- chi+ l WEIGHTED<=100
generate p p > chi- chi0 l WEIGHTED<=100
generate p p > chi- CHI0 l WEIGHTED<=100
generate p p > chi- phiQ l WEIGHTED<=100
generate p p > chi- PHIQ l WEIGHTED<=100
generate p p > chi- A0 l WEIGHTED<=100
generate p p > chi- S0 l WEIGHTED<=100
	generate p p > chi+ chi+ l WEIGHTED<=100
	generate p p > chi+ chi0 l WEIGHTED<=100
	generate p p > chi+ CHI0 l WEIGHTED<=100
	generate p p > chi+ phiQ l WEIGHTED<=100
	generate p p > chi+ PHIQ l WEIGHTED<=100
	generate p p > chi+ A0 l WEIGHTED<=100
	generate p p > chi+ S0 l WEIGHTED<=100

generate p p > chi0 chi0 l WEIGHTED<=100
generate p p > chi0 CHI0 l WEIGHTED<=100
generate p p > chi0 phiQ l WEIGHTED<=100
generate p p > chi0 PHIQ l WEIGHTED<=100
generate p p > chi0 A0 l WEIGHTED<=100
generate p p > chi0 S0 l WEIGHTED<=100
	generate p p > CHI0 CHI0 l WEIGHTED<=100
	generate p p > CHI0 phiQ l WEIGHTED<=100
	generate p p > CHI0 PHIQ l WEIGHTED<=100
	generate p p > CHI0 A0 l WEIGHTED<=100
	generate p p > CHI0 S0 l WEIGHTED<=100

generate p p > phiQ phiQ l WEIGHTED<=100
generate p p > phiQ PHIQ l WEIGHTED<=100
generate p p > phiQ A0 l WEIGHTED<=100
generate p p > phiQ S0 l WEIGHTED<=100
	generate p p > PHIQ PHIQ l WEIGHTED<=100
	generate p p > PHIQ A0 l WEIGHTED<=100
	generate p p > PHIQ S0 l WEIGHTED<=100

generate p p > A0 A0 l WEIGHTED<=100
generate p p > A0 S0 l WEIGHTED<=100

generate p p > S0 S0 l WEIGHTED<=100