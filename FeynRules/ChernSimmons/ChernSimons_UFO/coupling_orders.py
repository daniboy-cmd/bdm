# This file was automatically created by FeynRules 2.3.34
# Mathematica version: 11.1.0 for Microsoft Windows (64-bit) (March 13, 2017)
# Date: Tue 9 Jul 2019 13:05:15


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

NP = CouplingOrder(name = 'NP',
                   expansion_order = 99,
                   hierarchy = 1)

