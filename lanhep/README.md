## PDG definition
Currently pdg for the extra fields are:

|Particle|PDG   |
|--------|------|
|S0      |900006|
|A       |900007|
|x0      |900010|
|x-      |900009|
|Pq      |900008|


## Changes to *.mdl to make it work
- [ ] **aEWM1** and **Gf** must be **external** so you must choose the parameters like that ( in this model I had to replace EE and SW by these )
- [ ] need to set **parameters block information** using **lha param = BLOCK(INDEX)**. It should complain of this so if you forget one don't worry.
- [ ] make sure you do not give any parameter SMINPUTS(3) as 3 is already used by aS which is generated in the params even if you didnt i think

### Still not sure if needed changes
- [ ] Charges and lepton numbers of particles in **particles.py** not correct.
        The charges are solved by using **SetEM(A,EE).** but not the leptons.
- [ ] Coupling orders QED needs order 2.

## Creating the UFO files
Simply run 
```
../lanhep/lhep -ufo -OutDir directory bdm_ufo.mdl
```
*../lanhep/lhep* needs to point to where you have the executable *lhep* file and directory is the name of the folder it will create.

After that there are some issues and lanhep deosn't do a great job (also there's no documentation for how to properly write the model so there's that as well).
Thus some manual changes must be done. Compare the folders **bdm_UFO** and **bdm_manual_UFO** to see the differences but here's a list of what I have to do:
- [ ] Take out the duplicate aS parameter in file **parameters.py**. This one is probably due to badly written model.
```c++
...
aS = Parameter(name = 'aS',
           nature = 'external',
           type = 'real',
           value = 0.118,
           texname = '\\text{aS}',
           lhablock = 'SMINPUTS',
           lhacode = [3] )
...
```
After some research it appears at least one of these appears due to the redefinition of **GG** to a formula with **aS**.

- [ ] Lanhep has the bug where in the file **vertices.py** it will forget a '*' in the colour field between the terms. In this case it only happens once in **V_103** so it must be changed to:
```c++
...
V_103 = Vertex(name='V_103',
          particles = [  P.__tilde__Pq,  P.__tilde__Pq,  P.__tilde__pq,  P.__tilde__pq ],
          lorentz = [ L.SSSS_1  ],
          color = [ 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
          couplings = {(0,0):C.GC_61,(1,0):C.GC_61})
...
```

- [ ] I believe another bug is that in **particles.py** the mass and width appear as
```python
t =  Particle(  pdg_code = 6, (...)
              mass = 'Mt',
              width = 'wt',
```
When I went and checked other models it should look something like this:
```python
t =  Particle(  pdg_code = 6, (...)
              mass = Param.Mt,
              width = Param.wt,
```
So I used the script **fix_ufo** to replace all of those.
**Note that you must make sure that the parameters exist in **parameters.py**. For example in this case:
```python
Mt = Parameter(...)
```


Note that I've done this using the script **fix_ufo**.











