#	LanHEP output produced at Thu Dec  9 03:17:08 2021
#	from the file '/home/daniboy/bdm/bdm_ufo.mdl'
#	Model named 'B-anomaly DM Model (un. gauge)'


from object_library import all_lorentz, Lorentz
from function_library import complexconjugate, re, im, csc, sec, acsc, asec

UUV_1 = Lorentz(name = 'UUV_1',
                  spins = [-1, -1, 3],
                  structure = ' P(3,2)')

SSS_1 = Lorentz(name = 'SSS_1',
                  spins = [1, 1, 1],
                  structure = '1')

SSV_1 = Lorentz(name = 'SSV_1',
                  spins = [1, 1, 3],
                  structure = ' P(3,1)')

SSV_2 = Lorentz(name = 'SSV_2',
                  spins = [1, 1, 3],
                  structure = ' P(3,2)')

SVV_1 = Lorentz(name = 'SVV_1',
                  spins = [1, 3, 3],
                  structure = ' Metric(2,3)* P(-1,2)*P(-1,3)')

SVV_2 = Lorentz(name = 'SVV_2',
                  spins = [1, 3, 3],
                  structure = ' P(3,2)* P(2,3)')

SVV_3 = Lorentz(name = 'SVV_3',
                  spins = [1, 3, 3],
                  structure = ' Metric(2,3)')

FFS_1 = Lorentz(name = 'FFS_1',
                  spins = [2, 2, 1],
                  structure = 'ProjP(1,2)')

FFS_2 = Lorentz(name = 'FFS_2',
                  spins = [2, 2, 1],
                  structure = 'ProjM(1,2)')

FFV_1 = Lorentz(name = 'FFV_1',
                  spins = [2, 2, 3],
                  structure = 'Gamma(3,1,-1)*ProjP(-1,2)')

FFV_2 = Lorentz(name = 'FFV_2',
                  spins = [2, 2, 3],
                  structure = 'Gamma(3,1,-1)*ProjM(-1,2)')

FFV_3 = Lorentz(name = 'FFV_3',
                  spins = [2, 2, 3],
                  structure = 'Gamma(3,1,2)')

VVV_1 = Lorentz(name = 'VVV_1',
                  spins = [3, 3, 3],
                  structure = ' Metric(1,2)* P(3,2)')

VVV_2 = Lorentz(name = 'VVV_2',
                  spins = [3, 3, 3],
                  structure = ' Metric(2,3)* P(1,2)')

VVV_3 = Lorentz(name = 'VVV_3',
                  spins = [3, 3, 3],
                  structure = ' Metric(1,3)* P(2,3)')

VVV_4 = Lorentz(name = 'VVV_4',
                  spins = [3, 3, 3],
                  structure = ' Metric(2,3)* P(1,3)')

VVV_5 = Lorentz(name = 'VVV_5',
                  spins = [3, 3, 3],
                  structure = ' Metric(1,3)* P(2,1)')

VVV_6 = Lorentz(name = 'VVV_6',
                  spins = [3, 3, 3],
                  structure = ' Metric(1,2)* P(3,1)')

SSSS_1 = Lorentz(name = 'SSSS_1',
                  spins = [1, 1, 1, 1],
                  structure = '1')

SSVV_1 = Lorentz(name = 'SSVV_1',
                  spins = [1, 1, 3, 3],
                  structure = ' Metric(3,4)')

SVVV_1 = Lorentz(name = 'SVVV_1',
                  spins = [1, 3, 3, 3],
                  structure = ' Metric(2,4)* P(3,4)')

SVVV_2 = Lorentz(name = 'SVVV_2',
                  spins = [1, 3, 3, 3],
                  structure = ' Metric(3,4)* P(2,4)')

SVVV_3 = Lorentz(name = 'SVVV_3',
                  spins = [1, 3, 3, 3],
                  structure = ' Metric(2,3)* P(4,3)')

SVVV_4 = Lorentz(name = 'SVVV_4',
                  spins = [1, 3, 3, 3],
                  structure = ' Metric(3,4)* P(2,3)')

SVVV_5 = Lorentz(name = 'SVVV_5',
                  spins = [1, 3, 3, 3],
                  structure = ' Metric(2,3)* P(4,2)')

SVVV_6 = Lorentz(name = 'SVVV_6',
                  spins = [1, 3, 3, 3],
                  structure = ' Metric(2,4)* P(3,2)')

VVVV_1 = Lorentz(name = 'VVVV_1',
                  spins = [3, 3, 3, 3],
                  structure = ' Metric(1,2)* Metric(3,4)')

VVVV_2 = Lorentz(name = 'VVVV_2',
                  spins = [3, 3, 3, 3],
                  structure = ' Metric(1,3)* Metric(2,4)')

VVVV_3 = Lorentz(name = 'VVVV_3',
                  spins = [3, 3, 3, 3],
                  structure = ' Metric(1,4)* Metric(2,3)')

