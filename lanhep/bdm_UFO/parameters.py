#     LanHEP output produced at Mon Dec 20 20:21:25 2021
#     from the file '/home/daniboy/bdm/bdm_ufo.mdl'
#     Model named 'B-anomaly DM Model (un. gauge)'

from object_library import all_parameters, Parameter
from function_library import complexconjugate, re, im, csc, sec, acsc, asec

ZERO = Parameter(name = 'ZERO',
                  nature = 'internal',
                  type = 'real',
                  value = '0.0',
                  texname = '0')

Sqrt2 = Parameter(name = 'Sqrt2',
                  nature = 'internal',
                  type = 'real',
                  value = 'cmath.sqrt(2.0)',
                  texname = '\\sqrt{2}')

PI = Parameter(name = 'PI',
               nature = 'internal',
               type = 'real',
               value = 'cmath.acos(-1)',
               texname = '\\text{PI}' )

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = 'M_Z',
               lhablock = 'SMINPUTS',
               lhacode = [4] )

aEWM1 = Parameter(name = 'aEWM1',
               nature = 'external',
               type = 'real',
               value = 128,
               texname = '\\text{aEWM1}',
               lhablock = 'SMINPUTS',
               lhacode = [1] )

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 1.16639e-05,
               texname = '\\text{Gf}',
               lhablock = 'SMINPUTS',
               lhacode = [2] )

EE = Parameter(name = 'EE',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(4*PI/aEWM1)',
               texname = 'e' )

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ*MZ/2+cmath.sqrt(MZ*MZ*MZ*MZ/4-PI*(MZ*MZ)/(aEWM1*Gf*cmath.sqrt(2))))',
               texname = 'M_W' )

SW = Parameter(name = 'SW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1-MW*MW/(MZ*MZ))',
               texname = 's_w' )

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.118,
               texname = '\\text{aS}',
               lhablock = 'SMINPUTS',
               lhacode = [3] )

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.118,
               texname = '\\text{aS}',
               lhablock = 'SMINPUTS',
               lhacode = [3] )

GG = Parameter(name = 'GG',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(cmath.acos(-1)*aS)',
               texname = 'g_s' )

s12 = Parameter(name = 's12',
               nature = 'external',
               type = 'real',
               value = 0.2229,
               texname = '\\text{s12}',
               lhablock = 'CKM',
               lhacode = [1] )

s23 = Parameter(name = 's23',
               nature = 'external',
               type = 'real',
               value = 0.0412,
               texname = '\\text{s23}',
               lhablock = 'CKM',
               lhacode = [2] )

s13 = Parameter(name = 's13',
               nature = 'external',
               type = 'real',
               value = 0.0036,
               texname = '\\text{s13}',
               lhablock = 'CKM',
               lhacode = [3] )

CW = Parameter(name = 'CW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1-SW*SW)',
               texname = 'c_w' )

c12 = Parameter(name = 'c12',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1-s12*s12)',
               texname = '\\text{c12}' )

c23 = Parameter(name = 'c23',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1-s23*s23)',
               texname = '\\text{c23}' )

c13 = Parameter(name = 'c13',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1-s13*s13)',
               texname = '\\text{c13}' )

Vud = Parameter(name = 'Vud',
               nature = 'internal',
               type = 'real',
               value = 'c12*c13',
               texname = '\\text{Vud}' )

Vus = Parameter(name = 'Vus',
               nature = 'internal',
               type = 'real',
               value = 's12*c13',
               texname = '\\text{Vus}' )

Vub = Parameter(name = 'Vub',
               nature = 'internal',
               type = 'real',
               value = 's13',
               texname = '\\text{Vub}' )

Vcd = Parameter(name = 'Vcd',
               nature = 'internal',
               type = 'real',
               value = '-s12*c23-c12*s23*s13',
               texname = '\\text{Vcd}' )

Vcs = Parameter(name = 'Vcs',
               nature = 'internal',
               type = 'real',
               value = 'c12*c23-s12*s23*s13',
               texname = '\\text{Vcs}' )

Vcb = Parameter(name = 'Vcb',
               nature = 'internal',
               type = 'real',
               value = 's23*c13',
               texname = '\\text{Vcb}' )

Vtd = Parameter(name = 'Vtd',
               nature = 'internal',
               type = 'real',
               value = 's12*s23-c12*c23*s13',
               texname = '\\text{Vtd}' )

Vts = Parameter(name = 'Vts',
               nature = 'internal',
               type = 'real',
               value = '-c12*s23-s12*c23*s13',
               texname = '\\text{Vts}' )

Vtb = Parameter(name = 'Vtb',
               nature = 'internal',
               type = 'real',
               value = 'c23*c13',
               texname = '\\text{Vtb}' )

alfSMZ = Parameter(name = 'alfSMZ',
               nature = 'external',
               type = 'real',
               value = 0.1172,
               texname = '\\text{alfSMZ}',
               lhablock = 'SMINPUTS',
               lhacode = [5] )

Q = Parameter(name = 'Q',
               nature = 'external',
               type = 'real',
               value = 100,
               texname = '\\text{Q}',
               lhablock = 'SMINPUTS',
               lhacode = [6] )

Mtp = Parameter(name = 'Mtp',
               nature = 'external',
               type = 'real',
               value = 173.5,
               texname = '\\text{Mtp}',
               lhablock = 'MASS',
               lhacode = [1006] )

MbMb = Parameter(name = 'MbMb',
               nature = 'external',
               type = 'real',
               value = 4.23,
               texname = '\\text{MbMb}',
               lhablock = 'MASS',
               lhacode = [1005] )

McMc = Parameter(name = 'McMc',
               nature = 'external',
               type = 'real',
               value = 1.27,
               texname = '\\text{McMc}',
               lhablock = 'MASS',
               lhacode = [1004] )

initQCD51 = Parameter(name = 'initQCD51',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{initQCD51}',
               lhablock = 'SMINPUTS',
               lhacode = [7] )

Lqcd = Parameter(name = 'Lqcd',
               nature = 'internal',
               type = 'real',
               value = 'initQCD51',
               texname = '\\text{Lqcd}' )

MbEff1 = Parameter(name = 'MbEff1',
               nature = 'external',
               type = 'real',
               value = 3.2407,
               texname = '\\text{MbEff1}',
               lhablock = 'SMINPUTS',
               lhacode = [8] )

Mb = Parameter(name = 'Mb',
               nature = 'internal',
               type = 'real',
               value = 'MbEff1',
               texname = 'M_b' )

MtEff1 = Parameter(name = 'MtEff1',
               nature = 'external',
               type = 'real',
               value = 171.4,
               texname = '\\text{MtEff1}',
               lhablock = 'SMINPUTS',
               lhacode = [9] )

Mt = Parameter(name = 'Mt',
               nature = 'internal',
               type = 'real',
               value = 'MtEff1',
               texname = 'M_\tau' )

McEff1 = Parameter(name = 'McEff1',
               nature = 'external',
               type = 'real',
               value = 0.66065,
               texname = '\\text{McEff1}',
               lhablock = 'SMINPUTS',
               lhacode = [10] )

Mc = Parameter(name = 'Mc',
               nature = 'internal',
               type = 'real',
               value = 'McEff1',
               texname = 'M_c' )

alphaQCD1 = Parameter(name = 'alphaQCD1',
               nature = 'external',
               type = 'real',
               value = 0.1156,
               texname = '\\text{alphaQCD1}',
               lhablock = 'SMINPUTS',
               lhacode = [11] )

Mbp = Parameter(name = 'Mbp',
               nature = 'internal',
               type = 'real',
               value = 'MbMb*(1+4/3*alphaQCD1/PI)',
               texname = '\\text{Mbp}' )

alphaQCD2 = Parameter(name = 'alphaQCD2',
               nature = 'external',
               type = 'real',
               value = 0.1156,
               texname = '\\text{alphaQCD2}',
               lhablock = 'SMINPUTS',
               lhacode = [12] )

Mcp = Parameter(name = 'Mcp',
               nature = 'internal',
               type = 'real',
               value = 'Mc*(1+4/3*alphaQCD2/PI)',
               texname = '\\text{Mcp}' )

vH = Parameter(name = 'vH',
               nature = 'internal',
               type = 'real',
               value = '2*MW*SW/EE',
               texname = '\\text{vH}' )

Mh = Parameter(name = 'Mh',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{Mh}',
               lhablock = 'MASS',
               lhacode = [25] )

MS0 = Parameter(name = 'MS0',
               nature = 'external',
               type = 'real',
               value = 70,
               texname = '\\text{MS0}',
               lhablock = 'MASS',
               lhacode = [900006] )

MA = Parameter(name = 'MA',
               nature = 'external',
               type = 'real',
               value = 80,
               texname = '\\text{MA}',
               lhablock = 'MASS',
               lhacode = [900007] )

Mchi = Parameter(name = 'Mchi',
               nature = 'external',
               type = 'real',
               value = 200,
               texname = '\\text{Mchi}',
               lhablock = 'MASS',
               lhacode = [900009] )

MPq = Parameter(name = 'MPq',
               nature = 'external',
               type = 'real',
               value = 1000,
               texname = '\\text{MPq}',
               lhablock = 'MASS',
               lhacode = [900008] )

lamPq = Parameter(name = 'lamPq',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{lamPq}',
               lhablock = 'COUPLINGS',
               lhacode = [1] )

lamPl = Parameter(name = 'lamPl',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{lamPl}',
               lhablock = 'COUPLINGS',
               lhacode = [2] )

lamPlp = Parameter(name = 'lamPlp',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{lamPlp}',
               lhablock = 'COUPLINGS',
               lhacode = [3] )

kapHPq = Parameter(name = 'kapHPq',
               nature = 'external',
               type = 'real',
               value = 0.0001,
               texname = '\\text{kapHPq}',
               lhablock = 'COUPLINGS',
               lhacode = [4] )

kapHPl = Parameter(name = 'kapHPl',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{kapHPl}',
               lhablock = 'COUPLINGS',
               lhacode = [5] )

kapPqPl = Parameter(name = 'kapPqPl',
               nature = 'external',
               type = 'real',
               value = 0.001,
               texname = '\\text{kapPqPl}',
               lhablock = 'COUPLINGS',
               lhacode = [6] )

kapHPlp = Parameter(name = 'kapHPlp',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{kapHPlp}',
               lhablock = 'COUPLINGS',
               lhacode = [7] )

kapPqPlp = Parameter(name = 'kapPqPlp',
               nature = 'external',
               type = 'real',
               value = 0.001,
               texname = '\\text{kapPqPlp}',
               lhablock = 'COUPLINGS',
               lhacode = [8] )

yb = Parameter(name = 'yb',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{yb}',
               lhablock = 'YUKAWA',
               lhacode = [1] )

ys = Parameter(name = 'ys',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{ys}',
               lhablock = 'YUKAWA',
               lhacode = [2] )

ym = Parameter(name = 'ym',
               nature = 'external',
               type = 'real',
               value = 0.1,
               texname = '\\text{ym}',
               lhablock = 'YUKAWA',
               lhacode = [3] )

lamH = Parameter(name = 'lamH',
               nature = 'internal',
               type = 'real',
               value = 'Mh*Mh/(2*vH*vH)',
               texname = '\\text{lamH}' )

muH2 = Parameter(name = 'muH2',
               nature = 'internal',
               type = 'real',
               value = 'Mh*Mh/2',
               texname = '\\text{muH2}' )

muPl2 = Parameter(name = 'muPl2',
               nature = 'internal',
               type = 'real',
               value = '(MS0*MS0+MA*MA-kapHPl*vH*vH)/2',
               texname = '\\text{muPl2}' )

muPlp2 = Parameter(name = 'muPlp2',
               nature = 'internal',
               type = 'real',
               value = '(MS0*MS0-MA*MA-kapHPlp*vH*vH)/2',
               texname = '\\text{muPlp2}' )

muPq2 = Parameter(name = 'muPq2',
               nature = 'internal',
               type = 'real',
               value = 'MPq*MPq-kapHPq*(vH*vH)/2',
               texname = '\\text{muPq2}' )

wZ = Parameter(name = 'wZ',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\Gamma_Z',
               lhablock = 'DECAY',
               lhacode = [23] )

wW = Parameter(name = 'wW',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\Gamma_W',
               lhablock = 'DECAY',
               lhacode = [24] )

wxC = Parameter(name = 'wxC',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\\text{wxC}',
               lhablock = 'DECAY',
               lhacode = [900009] )

wx0 = Parameter(name = 'wx0',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\\text{wx0}',
               lhablock = 'DECAY',
               lhacode = [900010] )

wPq = Parameter(name = 'wPq',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\\text{wPq}',
               lhablock = 'DECAY',
               lhacode = [900008] )

wA = Parameter(name = 'wA',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\\text{wA}',
               lhablock = 'DECAY',
               lhacode = [900007] )

Me = Parameter(name = 'Me',
               nature = 'external',
               type = 'real',
               value = 0.000511,
               texname = 'M_e',
               lhablock = 'MASS',
               lhacode = [11] )

Mm = Parameter(name = 'Mm',
               nature = 'external',
               type = 'real',
               value = 0.10566,
               texname = 'M_\mu',
               lhablock = 'MASS',
               lhacode = [13] )

Ml = Parameter(name = 'Ml',
               nature = 'external',
               type = 'real',
               value = 1.777,
               texname = '\\text{Ml}',
               lhablock = 'MASS',
               lhacode = [15] )

Mu = Parameter(name = 'Mu',
               nature = 'external',
               type = 'real',
               value = 0.0023,
               texname = '\\text{Mu}',
               lhablock = 'MASS',
               lhacode = [2] )

Md = Parameter(name = 'Md',
               nature = 'external',
               type = 'real',
               value = 0.0048,
               texname = '\\text{Md}',
               lhablock = 'MASS',
               lhacode = [1] )

Ms = Parameter(name = 'Ms',
               nature = 'external',
               type = 'real',
               value = 0.117,
               texname = 'M_s',
               lhablock = 'MASS',
               lhacode = [3] )

wt = Parameter(name = 'wt',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\\text{wt}',
               lhablock = 'DECAY',
               lhacode = [6] )

wh = Parameter(name = 'wh',
               nature = 'external',
               type = 'real',
               value = 0,
               texname = '\\text{wh}',
               lhablock = 'DECAY',
               lhacode = [25] )

ahF_c = Parameter(name = 'ahF_c',
               nature = 'internal',
               type = 'real',
               value = '(-EE)/MW*Mc/SW/2/Mc',
               texname = '\\text{ahF_c}' )

ahF_b = Parameter(name = 'ahF_b',
               nature = 'internal',
               type = 'real',
               value = '(-EE)/MW*Mb/SW/2/Mb',
               texname = '\\text{ahF_b}' )

ahF_t = Parameter(name = 'ahF_t',
               nature = 'internal',
               type = 'real',
               value = '(-EE)/MW*Mt/SW/2/Mt',
               texname = '\\text{ahF_t}' )

ahF_l = Parameter(name = 'ahF_l',
               nature = 'internal',
               type = 'real',
               value = '(-EE)/MW*Ml/SW/2/Ml',
               texname = '\\text{ahF_l}' )

a_hV_W = Parameter(name = 'a_hV_W',
               nature = 'internal',
               type = 'real',
               value = 'EE*MW/SW/(MW*MW)',
               texname = '\\text{a_hV_W}' )

alphaQCD3 = Parameter(name = 'alphaQCD3',
               nature = 'external',
               type = 'real',
               value = 0.1156,
               texname = '\\text{alphaQCD3}',
               lhablock = 'SMINPUTS',
               lhacode = [13] )

aQCD_h = Parameter(name = 'aQCD_h',
               nature = 'internal',
               type = 'real',
               value = 'alphaQCD3/PI',
               texname = '\\text{aQCD_h}' )

Rqcd_h = Parameter(name = 'Rqcd_h',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1+aQCD_h*(149/12+aQCD_h*(68.6482-aQCD_h*212.447)))',
               texname = '\\text{Rqcd_h}' )

Quq = Parameter(name = 'Quq',
               nature = 'internal',
               type = 'real',
               value = '4/9',
               texname = '\\text{Quq}' )

Qdq = Parameter(name = 'Qdq',
               nature = 'internal',
               type = 'real',
               value = '1/9',
               texname = '\\text{Qdq}' )

hGGeven1 = Parameter(name = 'hGGeven1',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{hGGeven1}',
               lhablock = 'MISC',
               lhacode = [1] )

LGGH = Parameter(name = 'LGGH',
               nature = 'internal',
               type = 'real',
               value = '-abs(hGGeven1)',
               texname = '\\text{LGGH}' )

hAAeven1 = Parameter(name = 'hAAeven1',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{hAAeven1}',
               lhablock = 'MISC',
               lhacode = [2] )

hAAeven2 = Parameter(name = 'hAAeven2',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{hAAeven2}',
               lhablock = 'MISC',
               lhacode = [3] )

hAAeven3 = Parameter(name = 'hAAeven3',
               nature = 'external',
               type = 'real',
               value = 1,
               texname = '\\text{hAAeven3}',
               lhablock = 'MISC',
               lhacode = [4] )

LAAH = Parameter(name = 'LAAH',
               nature = 'internal',
               type = 'real',
               value = '-abs(Qdq*hAAeven1+Quq*hAAeven2+hAAeven3)',
               texname = '\\text{LAAH}' )

