#	LanHEP output produced at Mon Dec 20 20:21:25 2021
#	from the file '/home/daniboy/bdm/bdm_ufo.mdl'
#	Model named 'B-anomaly DM Model (un. gauge)'


from object_library import all_vertices, Vertex
import particles as P
import couplings as C
import lorentz as L

V_1 = Vertex(name='V_1',
              particles = [  P.Ggh__tilde__,  P.Ggh,  P.G ],
              lorentz = [ L.UUV_1  ],
              color = [ 'f(1,2,3)' ],
              couplings = {(0,0):C.GC_1})

V_2 = Vertex(name='V_2',
              particles = [  P.h,  P.h,  P.h ],
              lorentz = [ L.SSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_2})

V_3 = Vertex(name='V_3',
              particles = [  P.h,  P.__tilde__A,  P.__tilde__A ],
              lorentz = [ L.SSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_3})

V_4 = Vertex(name='V_4',
              particles = [  P.h,  P.__tilde__Pq,  P.__tilde__pq ],
              lorentz = [ L.SSS_1  ],
              color = [ 'Identity(2,3)' ],
              couplings = {(0,0):C.GC_4})

V_5 = Vertex(name='V_5',
              particles = [  P.h,  P.__tilde__S,  P.__tilde__S ],
              lorentz = [ L.SSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_5})

V_6 = Vertex(name='V_6',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.A ],
              lorentz = [ L.SSV_1, L.SSV_2  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_6,(0,1):C.GC_7})

V_7 = Vertex(name='V_7',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.G ],
              lorentz = [ L.SSV_1, L.SSV_2  ],
              color = [ 'T(3,2,1)' ],
              couplings = {(0,0):C.GC_8,(0,1):C.GC_9})

V_8 = Vertex(name='V_8',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.Z ],
              lorentz = [ L.SSV_1, L.SSV_2  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_10,(0,1):C.GC_11})

V_9 = Vertex(name='V_9',
              particles = [  P.h,  P.A,  P.A ],
              lorentz = [ L.SVV_1, L.SVV_2, L.SVV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_12,(0,1):C.GC_13})

V_10 = Vertex(name='V_10',
              particles = [  P.h,  P.G,  P.G ],
              lorentz = [ L.SVV_1, L.SVV_2, L.SVV_3  ],
              color = [ 'Identity(2,3)' ],
              couplings = {(0,0):C.GC_14,(0,1):C.GC_15})

V_11 = Vertex(name='V_11',
              particles = [  P.h,  P.W__plus__,  P.W__minus__ ],
              lorentz = [ L.SVV_1, L.SVV_2, L.SVV_3  ],
              color = [ '1' ],
              couplings = {(0,2):C.GC_16})

V_12 = Vertex(name='V_12',
              particles = [  P.h,  P.Z,  P.Z ],
              lorentz = [ L.SVV_1, L.SVV_2, L.SVV_3  ],
              color = [ '1' ],
              couplings = {(0,2):C.GC_17})

V_13 = Vertex(name='V_13',
              particles = [  P.B,  P.b,  P.h ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_18,(0,1):C.GC_18})

V_14 = Vertex(name='V_14',
              particles = [  P.B,  P.__tilde__x__minus__,  P.__tilde__pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,3)' ],
              couplings = {(0,0):C.GC_19})

V_15 = Vertex(name='V_15',
              particles = [  P.C,  P.c,  P.h ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_20,(0,1):C.GC_20})

V_16 = Vertex(name='V_16',
              particles = [  P.C,  P.__tilde__x0,  P.__tilde__pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,3)' ],
              couplings = {(0,0):C.GC_21})

V_17 = Vertex(name='V_17',
              particles = [  P.L,  P.l,  P.h ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_22,(0,1):C.GC_22})

V_18 = Vertex(name='V_18',
              particles = [  P.M,  P.m,  P.h ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_23,(0,1):C.GC_23})

V_19 = Vertex(name='V_19',
              particles = [  P.M,  P.__tilde__x__minus__,  P.__tilde__A ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_24})

V_20 = Vertex(name='V_20',
              particles = [  P.M,  P.__tilde__x__minus__,  P.__tilde__S ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_25})

V_21 = Vertex(name='V_21',
              particles = [  P.Nm,  P.__tilde__x0,  P.__tilde__A ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_24})

V_22 = Vertex(name='V_22',
              particles = [  P.Nm,  P.__tilde__x0,  P.__tilde__S ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_25})

V_23 = Vertex(name='V_23',
              particles = [  P.S,  P.s,  P.h ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_26,(0,1):C.GC_26})

V_24 = Vertex(name='V_24',
              particles = [  P.S,  P.__tilde__x__minus__,  P.__tilde__pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,3)' ],
              couplings = {(0,0):C.GC_27})

V_25 = Vertex(name='V_25',
              particles = [  P.T,  P.t,  P.h ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_28,(0,1):C.GC_28})

V_26 = Vertex(name='V_26',
              particles = [  P.T,  P.__tilde__x0,  P.__tilde__pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,3)' ],
              couplings = {(0,0):C.GC_29})

V_27 = Vertex(name='V_27',
              particles = [  P.U,  P.__tilde__x0,  P.__tilde__pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(1,3)' ],
              couplings = {(0,0):C.GC_30})

V_28 = Vertex(name='V_28',
              particles = [  P.__tilde__X__plus__,  P.b,  P.__tilde__Pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(2,3)' ],
              couplings = {(0,1):C.GC_19})

V_29 = Vertex(name='V_29',
              particles = [  P.__tilde__X__plus__,  P.m,  P.__tilde__A ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_31})

V_30 = Vertex(name='V_30',
              particles = [  P.__tilde__X__plus__,  P.m,  P.__tilde__S ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_25})

V_31 = Vertex(name='V_31',
              particles = [  P.__tilde__X__plus__,  P.s,  P.__tilde__Pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(2,3)' ],
              couplings = {(0,1):C.GC_27})

V_32 = Vertex(name='V_32',
              particles = [  P.__tilde__X0,  P.c,  P.__tilde__Pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(2,3)' ],
              couplings = {(0,1):C.GC_21})

V_33 = Vertex(name='V_33',
              particles = [  P.__tilde__X0,  P.nm,  P.__tilde__A ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_31})

V_34 = Vertex(name='V_34',
              particles = [  P.__tilde__X0,  P.nm,  P.__tilde__S ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_25})

V_35 = Vertex(name='V_35',
              particles = [  P.__tilde__X0,  P.t,  P.__tilde__Pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(2,3)' ],
              couplings = {(0,1):C.GC_29})

V_36 = Vertex(name='V_36',
              particles = [  P.__tilde__X0,  P.u,  P.__tilde__Pq ],
              lorentz = [ L.FFS_1, L.FFS_2  ],
              color = [ 'Identity(2,3)' ],
              couplings = {(0,1):C.GC_30})

V_37 = Vertex(name='V_37',
              particles = [  P.B,  P.b,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_32,(0,1):C.GC_32})

V_38 = Vertex(name='V_38',
              particles = [  P.B,  P.b,  P.G ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'T(3,2,1)' ],
              couplings = {(0,2):C.GC_9})

V_39 = Vertex(name='V_39',
              particles = [  P.B,  P.b,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_33,(0,1):C.GC_34})

V_40 = Vertex(name='V_40',
              particles = [  P.B,  P.c,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_35})

V_41 = Vertex(name='V_41',
              particles = [  P.B,  P.t,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_36})

V_42 = Vertex(name='V_42',
              particles = [  P.B,  P.u,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_37})

V_43 = Vertex(name='V_43',
              particles = [  P.C,  P.b,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_35})

V_44 = Vertex(name='V_44',
              particles = [  P.C,  P.c,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_7,(0,1):C.GC_7})

V_45 = Vertex(name='V_45',
              particles = [  P.C,  P.c,  P.G ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'T(3,2,1)' ],
              couplings = {(0,2):C.GC_9})

V_46 = Vertex(name='V_46',
              particles = [  P.C,  P.c,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_11,(0,1):C.GC_38})

V_47 = Vertex(name='V_47',
              particles = [  P.C,  P.d,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_39})

V_48 = Vertex(name='V_48',
              particles = [  P.C,  P.s,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_40})

V_49 = Vertex(name='V_49',
              particles = [  P.D,  P.c,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_39})

V_50 = Vertex(name='V_50',
              particles = [  P.D,  P.d,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_32,(0,1):C.GC_32})

V_51 = Vertex(name='V_51',
              particles = [  P.D,  P.d,  P.G ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'T(3,2,1)' ],
              couplings = {(0,2):C.GC_9})

V_52 = Vertex(name='V_52',
              particles = [  P.D,  P.d,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_33,(0,1):C.GC_34})

V_53 = Vertex(name='V_53',
              particles = [  P.D,  P.t,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_41})

V_54 = Vertex(name='V_54',
              particles = [  P.D,  P.u,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_42})

V_55 = Vertex(name='V_55',
              particles = [  P.E,  P.e,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_43,(0,1):C.GC_43})

V_56 = Vertex(name='V_56',
              particles = [  P.E,  P.e,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_45,(0,1):C.GC_44})

V_57 = Vertex(name='V_57',
              particles = [  P.E,  P.ne,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_46})

V_58 = Vertex(name='V_58',
              particles = [  P.L,  P.l,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_43,(0,1):C.GC_43})

V_59 = Vertex(name='V_59',
              particles = [  P.L,  P.l,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_45,(0,1):C.GC_44})

V_60 = Vertex(name='V_60',
              particles = [  P.L,  P.nl,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_46})

V_61 = Vertex(name='V_61',
              particles = [  P.M,  P.m,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_43,(0,1):C.GC_43})

V_62 = Vertex(name='V_62',
              particles = [  P.M,  P.m,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_45,(0,1):C.GC_44})

V_63 = Vertex(name='V_63',
              particles = [  P.M,  P.nm,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_46})

V_64 = Vertex(name='V_64',
              particles = [  P.Ne,  P.e,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_46})

V_65 = Vertex(name='V_65',
              particles = [  P.Ne,  P.ne,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_47})

V_66 = Vertex(name='V_66',
              particles = [  P.Nl,  P.l,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_46})

V_67 = Vertex(name='V_67',
              particles = [  P.Nl,  P.nl,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_47})

V_68 = Vertex(name='V_68',
              particles = [  P.Nm,  P.m,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_46})

V_69 = Vertex(name='V_69',
              particles = [  P.Nm,  P.nm,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,1):C.GC_47})

V_70 = Vertex(name='V_70',
              particles = [  P.S,  P.c,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_40})

V_71 = Vertex(name='V_71',
              particles = [  P.S,  P.s,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_32,(0,1):C.GC_32})

V_72 = Vertex(name='V_72',
              particles = [  P.S,  P.s,  P.G ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'T(3,2,1)' ],
              couplings = {(0,2):C.GC_9})

V_73 = Vertex(name='V_73',
              particles = [  P.S,  P.s,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_33,(0,1):C.GC_34})

V_74 = Vertex(name='V_74',
              particles = [  P.S,  P.t,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_48})

V_75 = Vertex(name='V_75',
              particles = [  P.S,  P.u,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_49})

V_76 = Vertex(name='V_76',
              particles = [  P.T,  P.b,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_36})

V_77 = Vertex(name='V_77',
              particles = [  P.T,  P.d,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_41})

V_78 = Vertex(name='V_78',
              particles = [  P.T,  P.s,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_48})

V_79 = Vertex(name='V_79',
              particles = [  P.T,  P.t,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_7,(0,1):C.GC_7})

V_80 = Vertex(name='V_80',
              particles = [  P.T,  P.t,  P.G ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'T(3,2,1)' ],
              couplings = {(0,2):C.GC_9})

V_81 = Vertex(name='V_81',
              particles = [  P.T,  P.t,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_11,(0,1):C.GC_38})

V_82 = Vertex(name='V_82',
              particles = [  P.U,  P.b,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_37})

V_83 = Vertex(name='V_83',
              particles = [  P.U,  P.d,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_42})

V_84 = Vertex(name='V_84',
              particles = [  P.U,  P.s,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,1):C.GC_49})

V_85 = Vertex(name='V_85',
              particles = [  P.U,  P.u,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_7,(0,1):C.GC_7})

V_86 = Vertex(name='V_86',
              particles = [  P.U,  P.u,  P.G ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'T(3,2,1)' ],
              couplings = {(0,2):C.GC_9})

V_87 = Vertex(name='V_87',
              particles = [  P.U,  P.u,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_11,(0,1):C.GC_38})

V_88 = Vertex(name='V_88',
              particles = [  P.__tilde__X__plus__,  P.__tilde__x__minus__,  P.A ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,2):C.GC_43})

V_89 = Vertex(name='V_89',
              particles = [  P.__tilde__X__plus__,  P.__tilde__x__minus__,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,2):C.GC_44})

V_90 = Vertex(name='V_90',
              particles = [  P.__tilde__X__plus__,  P.__tilde__x0,  P.W__minus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,2):C.GC_46})

V_91 = Vertex(name='V_91',
              particles = [  P.__tilde__X0,  P.__tilde__x__minus__,  P.W__plus__ ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,2):C.GC_46})

V_92 = Vertex(name='V_92',
              particles = [  P.__tilde__X0,  P.__tilde__x0,  P.Z ],
              lorentz = [ L.FFV_1, L.FFV_2, L.FFV_3  ],
              color = [ '1' ],
              couplings = {(0,2):C.GC_47})

V_93 = Vertex(name='V_93',
              particles = [  P.A,  P.W__plus__,  P.W__minus__ ],
              lorentz = [ L.VVV_1, L.VVV_2, L.VVV_3, L.VVV_4, L.VVV_5, L.VVV_6  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_50,(0,1):C.GC_43,(0,2):C.GC_43,(0,3):C.GC_50,(0,4):C.GC_50,(0,5):C.GC_43})

V_94 = Vertex(name='V_94',
              particles = [  P.G,  P.G,  P.G ],
              lorentz = [ L.VVV_1, L.VVV_2, L.VVV_3, L.VVV_4, L.VVV_5, L.VVV_6  ],
              color = [ 'f(1,2,3)' ],
              couplings = {(0,0):C.GC_51,(0,1):C.GC_1,(0,2):C.GC_1,(0,3):C.GC_51,(0,4):C.GC_51,(0,5):C.GC_1})

V_95 = Vertex(name='V_95',
              particles = [  P.W__plus__,  P.W__minus__,  P.Z ],
              lorentz = [ L.VVV_1, L.VVV_2, L.VVV_3, L.VVV_4, L.VVV_5, L.VVV_6  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_52,(0,1):C.GC_53,(0,2):C.GC_53,(0,3):C.GC_52,(0,4):C.GC_52,(0,5):C.GC_53})

V_96 = Vertex(name='V_96',
              particles = [  P.h,  P.h,  P.h,  P.h ],
              lorentz = [ L.SSSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_54})

V_97 = Vertex(name='V_97',
              particles = [  P.h,  P.h,  P.__tilde__A,  P.__tilde__A ],
              lorentz = [ L.SSSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_55})

V_98 = Vertex(name='V_98',
              particles = [  P.h,  P.h,  P.__tilde__Pq,  P.__tilde__pq ],
              lorentz = [ L.SSSS_1  ],
              color = [ 'Identity(3,4)' ],
              couplings = {(0,0):C.GC_56})

V_99 = Vertex(name='V_99',
              particles = [  P.h,  P.h,  P.__tilde__S,  P.__tilde__S ],
              lorentz = [ L.SSSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_57})

V_100 = Vertex(name='V_100',
              particles = [  P.__tilde__A,  P.__tilde__A,  P.__tilde__A,  P.__tilde__A ],
              lorentz = [ L.SSSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_58})

V_101 = Vertex(name='V_101',
              particles = [  P.__tilde__A,  P.__tilde__A,  P.__tilde__Pq,  P.__tilde__pq ],
              lorentz = [ L.SSSS_1  ],
              color = [ 'Identity(3,4)' ],
              couplings = {(0,0):C.GC_59})

V_102 = Vertex(name='V_102',
              particles = [  P.__tilde__A,  P.__tilde__A,  P.__tilde__S,  P.__tilde__S ],
              lorentz = [ L.SSSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_60})

V_103 = Vertex(name='V_103',
              particles = [  P.__tilde__Pq,  P.__tilde__Pq,  P.__tilde__pq,  P.__tilde__pq ],
              lorentz = [ L.SSSS_1  ],
              color = [ 'Identity(1,3)Identity(2,4)', 'Identity(1,4)Identity(2,3)' ],
              couplings = {(0,0):C.GC_61,(1,0):C.GC_61})

V_104 = Vertex(name='V_104',
              particles = [  P.__tilde__Pq,  P.__tilde__S,  P.__tilde__S,  P.__tilde__pq ],
              lorentz = [ L.SSSS_1  ],
              color = [ 'Identity(1,4)' ],
              couplings = {(0,0):C.GC_62})

V_105 = Vertex(name='V_105',
              particles = [  P.__tilde__S,  P.__tilde__S,  P.__tilde__S,  P.__tilde__S ],
              lorentz = [ L.SSSS_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_58})

V_106 = Vertex(name='V_106',
              particles = [  P.h,  P.h,  P.W__plus__,  P.W__minus__ ],
              lorentz = [ L.SSVV_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_63})

V_107 = Vertex(name='V_107',
              particles = [  P.h,  P.h,  P.Z,  P.Z ],
              lorentz = [ L.SSVV_1  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_64})

V_108 = Vertex(name='V_108',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.A,  P.A ],
              lorentz = [ L.SSVV_1  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_65})

V_109 = Vertex(name='V_109',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.A,  P.G ],
              lorentz = [ L.SSVV_1  ],
              color = [ 'T(4,2,1)' ],
              couplings = {(0,0):C.GC_66})

V_110 = Vertex(name='V_110',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.A,  P.Z ],
              lorentz = [ L.SSVV_1  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_67})

V_111 = Vertex(name='V_111',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.G,  P.G ],
              lorentz = [ L.SSVV_1  ],
              color = [ 'T(3,2,-1)*T(4,-1,1)', 'T(4,2,-1)*T(3,-1,1)' ],
              couplings = {(0,0):C.GC_68,(1,0):C.GC_68})

V_112 = Vertex(name='V_112',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.G,  P.Z ],
              lorentz = [ L.SSVV_1  ],
              color = [ 'T(3,2,1)' ],
              couplings = {(0,0):C.GC_69})

V_113 = Vertex(name='V_113',
              particles = [  P.__tilde__Pq,  P.__tilde__pq,  P.Z,  P.Z ],
              lorentz = [ L.SSVV_1  ],
              color = [ 'Identity(1,2)' ],
              couplings = {(0,0):C.GC_70})

V_114 = Vertex(name='V_114',
              particles = [  P.h,  P.G,  P.G,  P.G ],
              lorentz = [ L.SVVV_1, L.SVVV_2, L.SVVV_3, L.SVVV_4, L.SVVV_5, L.SVVV_6  ],
              color = [ 'f(2,3,4)' ],
              couplings = {(0,0):C.GC_71,(0,1):C.GC_72,(0,2):C.GC_72,(0,3):C.GC_71,(0,4):C.GC_71,(0,5):C.GC_72})

V_115 = Vertex(name='V_115',
              particles = [  P.A,  P.A,  P.W__plus__,  P.W__minus__ ],
              lorentz = [ L.VVVV_1, L.VVVV_2, L.VVVV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_73,(0,1):C.GC_74,(0,2):C.GC_74})

V_116 = Vertex(name='V_116',
              particles = [  P.A,  P.W__plus__,  P.W__minus__,  P.Z ],
              lorentz = [ L.VVVV_1, L.VVVV_2, L.VVVV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_76,(0,1):C.GC_76,(0,2):C.GC_75})

V_117 = Vertex(name='V_117',
              particles = [  P.G,  P.G,  P.G,  P.G ],
              lorentz = [ L.VVVV_1, L.VVVV_2, L.VVVV_3  ],
              color = [ 'f(1,2,-1)*f(3,4,-1)', 'f(1,3,-1)*f(2,4,-1)', 'f(1,4,-1)*f(2,3,-1)' ],
              couplings = {(1,0):C.GC_77,(2,0):C.GC_77,(0,1):C.GC_77,(2,1):C.GC_68,(0,2):C.GC_68,(1,2):C.GC_68})

V_118 = Vertex(name='V_118',
              particles = [  P.W__plus__,  P.W__plus__,  P.W__minus__,  P.W__minus__ ],
              lorentz = [ L.VVVV_1, L.VVVV_2, L.VVVV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_78,(0,1):C.GC_79,(0,2):C.GC_79})

V_119 = Vertex(name='V_119',
              particles = [  P.W__plus__,  P.W__minus__,  P.Z,  P.Z ],
              lorentz = [ L.VVVV_1, L.VVVV_2, L.VVVV_3  ],
              color = [ '1' ],
              couplings = {(0,0):C.GC_80,(0,1):C.GC_81,(0,2):C.GC_81})

