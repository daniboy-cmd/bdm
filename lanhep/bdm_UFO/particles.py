#	LanHEP output produced at Mon Dec 20 20:21:25 2021
#	from the file '/home/daniboy/bdm/bdm_ufo.mdl'
#	Model named 'B-anomaly DM Model (un. gauge)'


from __future__ import division
from object_library import all_particles, Particle
import propagators as Prop

Ggh =  Particle(  pdg_code = 0,
              name = 'G.c',
              antiname = 'G.C',
              spin = -1,
              color = 8,
              mass = 'ZERO',
              width = 'ZERO',
              texname = 'C^G',
              antitexname = '\bar{C}^G',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 1)

Ggh__tilde__ = Ggh.anti()

G =  Particle(  pdg_code = 21,
              name = 'G',
              antiname = 'G',
              spin = 3,
              color = 8,
              mass = 'ZERO',
              width = 'ZERO',
              texname = 'G',
              antitexname = 'G',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

Agh =  Particle(  pdg_code = 0,
              name = 'A.c',
              antiname = 'A.C',
              spin = -1,
              color = 1,
              mass = 'ZERO',
              width = 'ZERO',
              texname = 'C^{Z}',
              antitexname = '\bar{C}^A',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 1)

Agh__tilde__ = Agh.anti()

A =  Particle(  pdg_code = 22,
              name = 'A',
              antiname = 'A',
              spin = 3,
              color = 1,
              mass = 'ZERO',
              width = 'ZERO',
              texname = 'A',
              antitexname = 'A',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

Z =  Particle(  pdg_code = 23,
              name = 'Z',
              antiname = 'Z',
              spin = 3,
              color = 1,
              mass = 'MZ',
              width = 'wZ',
              texname = 'Z',
              antitexname = 'Z',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

W__plus__ =  Particle(  pdg_code = 24,
              name = 'W+',
              antiname = 'W-',
              spin = 3,
              color = 1,
              mass = 'MW',
              width = 'wW',
              texname = 'W^+',
              antitexname = 'W^-',
              charge = 1,
              LeptonNumber = 0,
              GhostNumber = 0)

W__minus__ = W__plus__.anti()

__tilde__x__minus__ =  Particle(  pdg_code = 900009,
              name = '~x-',
              antiname = '~X+',
              spin = 2,
              color = 1,
              mass = 'Mchi',
              width = 'wxC',
              texname = '~x-',
              antitexname = '~X+',
              charge = -1,
              LeptonNumber = 0,
              GhostNumber = 0)

__tilde__X__plus__ = __tilde__x__minus__.anti()

__tilde__x0 =  Particle(  pdg_code = 900010,
              name = '~x0',
              antiname = '~X0',
              spin = 2,
              color = 1,
              mass = 'Mchi',
              width = 'wx0',
              texname = '~x0',
              antitexname = '~X0',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

__tilde__X0 = __tilde__x0.anti()

__tilde__pq =  Particle(  pdg_code = 900008,
              name = '~pq',
              antiname = '~Pq',
              spin = 1,
              color = 3,
              mass = 'MPq',
              width = 'wPq',
              texname = '~pq',
              antitexname = '~Pq',
              charge = 2/3,
              LeptonNumber = 0,
              GhostNumber = 0)

__tilde__Pq = __tilde__pq.anti()

__tilde__S =  Particle(  pdg_code = 900006,
              name = '~S',
              antiname = '~S',
              spin = 1,
              color = 1,
              mass = 'MS0',
              width = 'ZERO',
              texname = '~S',
              antitexname = '~S',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

__tilde__A =  Particle(  pdg_code = 900007,
              name = '~A',
              antiname = '~A',
              spin = 1,
              color = 1,
              mass = 'MA',
              width = 'wA',
              texname = '~A',
              antitexname = '~A',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

ne =  Particle(  pdg_code = 12,
              name = 'ne',
              antiname = 'Ne',
              spin = 2,
              color = 1,
              mass = 'ZERO',
              width = 'ZERO',
              texname = '\nu^e',
              antitexname = '\bar{\nu}^e',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

Ne = ne.anti()

e =  Particle(  pdg_code = 11,
              name = 'e',
              antiname = 'E',
              spin = 2,
              color = 1,
              mass = 'Me',
              width = 'ZERO',
              texname = 'e',
              antitexname = '\bar{e}',
              charge = -1,
              LeptonNumber = 0,
              GhostNumber = 0)

E = e.anti()

nm =  Particle(  pdg_code = 14,
              name = 'nm',
              antiname = 'Nm',
              spin = 2,
              color = 1,
              mass = 'ZERO',
              width = 'ZERO',
              texname = '\nu^\mu',
              antitexname = '\bar{\nu}^\mu',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

Nm = nm.anti()

m =  Particle(  pdg_code = 13,
              name = 'm',
              antiname = 'M',
              spin = 2,
              color = 1,
              mass = 'Mm',
              width = 'ZERO',
              texname = '\mu',
              antitexname = '\bar{\mu}',
              charge = -1,
              LeptonNumber = 0,
              GhostNumber = 0)

M = m.anti()

nl =  Particle(  pdg_code = 16,
              name = 'nl',
              antiname = 'Nl',
              spin = 2,
              color = 1,
              mass = 'ZERO',
              width = 'ZERO',
              texname = '\nu^\tau',
              antitexname = '\bar{\nu}^\tau',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

Nl = nl.anti()

l =  Particle(  pdg_code = 15,
              name = 'l',
              antiname = 'L',
              spin = 2,
              color = 1,
              mass = 'Ml',
              width = 'ZERO',
              texname = '\tau',
              antitexname = '\bar{\tau}',
              charge = -1,
              LeptonNumber = 0,
              GhostNumber = 0)

L = l.anti()

u =  Particle(  pdg_code = 2,
              name = 'u',
              antiname = 'U',
              spin = 2,
              color = 3,
              mass = 'Mu',
              width = 'ZERO',
              texname = 'u',
              antitexname = '\bar{u}',
              charge = 2/3,
              LeptonNumber = 0,
              GhostNumber = 0)

U = u.anti()

d =  Particle(  pdg_code = 1,
              name = 'd',
              antiname = 'D',
              spin = 2,
              color = 3,
              mass = 'Md',
              width = 'ZERO',
              texname = 'd',
              antitexname = '\bar{d}',
              charge = -1/3,
              LeptonNumber = 0,
              GhostNumber = 0)

D = d.anti()

c =  Particle(  pdg_code = 4,
              name = 'c',
              antiname = 'C',
              spin = 2,
              color = 3,
              mass = 'Mc',
              width = 'ZERO',
              texname = 'c',
              antitexname = '\bar{c}',
              charge = 2/3,
              LeptonNumber = 0,
              GhostNumber = 0)

C = c.anti()

s =  Particle(  pdg_code = 3,
              name = 's',
              antiname = 'S',
              spin = 2,
              color = 3,
              mass = 'Ms',
              width = 'ZERO',
              texname = 's',
              antitexname = '\bar{s}',
              charge = -1/3,
              LeptonNumber = 0,
              GhostNumber = 0)

S = s.anti()

t =  Particle(  pdg_code = 6,
              name = 't',
              antiname = 'T',
              spin = 2,
              color = 3,
              mass = 'Mt',
              width = 'wt',
              texname = 't',
              antitexname = '\bar{t}',
              charge = 2/3,
              LeptonNumber = 0,
              GhostNumber = 0)

T = t.anti()

b =  Particle(  pdg_code = 5,
              name = 'b',
              antiname = 'B',
              spin = 2,
              color = 3,
              mass = 'Mb',
              width = 'ZERO',
              texname = 'b',
              antitexname = '\bar{b}',
              charge = -1/3,
              LeptonNumber = 0,
              GhostNumber = 0)

B = b.anti()

h =  Particle(  pdg_code = 25,
              name = 'h',
              antiname = 'h',
              spin = 1,
              color = 1,
              mass = 'Mh',
              width = 'wh',
              texname = 'h',
              antitexname = 'h',
              charge = 0,
              LeptonNumber = 0,
              GhostNumber = 0)

