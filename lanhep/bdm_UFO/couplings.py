#     LanHEP output produced at Mon Dec 20 20:21:25 2021
#     from the file '/home/daniboy/bdm/bdm_ufo.mdl'
#     Model named 'B-anomaly DM Model (un. gauge)'

from object_library import all_couplings, Coupling
from function_library import complexconjugate, re, im, csc, sec, acsc, asec

GC_1 = Coupling(name = 'GC_1',
               value = '-GG',
               order = {'QCD':1 })

GC_2 = Coupling(name = 'GC_2',
               value = '-12*complex(0,1)/EE*MW*SW*lamH',
               order = { })

GC_3 = Coupling(name = 'GC_3',
               value = 'complex(0,1)/EE*MW*SW*(-2*kapHPl+2*kapHPlp)',
               order = { })

GC_4 = Coupling(name = 'GC_4',
               value = '-2*complex(0,1)/EE*MW*SW*kapHPq',
               order = { })

GC_5 = Coupling(name = 'GC_5',
               value = 'complex(0,1)/EE*MW*SW*(-2*kapHPl-2*kapHPlp)',
               order = { })

GC_6 = Coupling(name = 'GC_6',
               value = '2*complex(0,1)*EE/3',
               order = {'QED':1 })

GC_7 = Coupling(name = 'GC_7',
               value = '-2*complex(0,1)*EE/3',
               order = {'QED':1 })

GC_8 = Coupling(name = 'GC_8',
               value = 'complex(0,1)*GG',
               order = {'QCD':1 })

GC_9 = Coupling(name = 'GC_9',
               value = '-complex(0,1)*GG',
               order = {'QCD':1 })

GC_10 = Coupling(name = 'GC_10',
               value = '-2*complex(0,1)/CW*EE*SW/3',
               order = {'QED':1 })

GC_11 = Coupling(name = 'GC_11',
               value = '2*complex(0,1)/CW*EE*SW/3',
               order = {'QED':1 })

GC_12 = Coupling(name = 'GC_12',
               value = '-4*complex(0,1)*LAAH',
               order = { })

GC_13 = Coupling(name = 'GC_13',
               value = '4*complex(0,1)*LAAH',
               order = { })

GC_14 = Coupling(name = 'GC_14',
               value = '-4*complex(0,1)*LGGH*Rqcd_h',
               order = { })

GC_15 = Coupling(name = 'GC_15',
               value = '4*complex(0,1)*LGGH*Rqcd_h',
               order = { })

GC_16 = Coupling(name = 'GC_16',
               value = 'complex(0,1)*EE*MW/SW',
               order = {'QED':1 })

GC_17 = Coupling(name = 'GC_17',
               value = 'complex(0,1)*EE*MW*(2*SW+SW*SW*SW/(CW*CW)+CW*CW/SW)',
               order = {'QED':1 })

GC_18 = Coupling(name = 'GC_18',
               value = '-complex(0,1)*EE/MW*Mb/SW/2',
               order = {'QED':1 })

GC_19 = Coupling(name = 'GC_19',
               value = 'complex(0,1)*yb',
               order = { })

GC_20 = Coupling(name = 'GC_20',
               value = '-complex(0,1)*EE/MW*Mc/SW/2',
               order = {'QED':1 })

GC_21 = Coupling(name = 'GC_21',
               value = 'complex(0,1)*(Vcb*yb+Vcs*ys)',
               order = { })

GC_22 = Coupling(name = 'GC_22',
               value = '-complex(0,1)*EE/MW*Ml/SW/2',
               order = {'QED':1 })

GC_23 = Coupling(name = 'GC_23',
               value = '-complex(0,1)*EE/MW*Mm/SW/2',
               order = {'QED':1 })

GC_24 = Coupling(name = 'GC_24',
               value = '-Sqrt2*ym/2',
               order = { })

GC_25 = Coupling(name = 'GC_25',
               value = 'complex(0,1)*Sqrt2*ym/2',
               order = { })

GC_26 = Coupling(name = 'GC_26',
               value = '-complex(0,1)*EE/MW*Ms/SW/2',
               order = {'QED':1 })

GC_27 = Coupling(name = 'GC_27',
               value = 'complex(0,1)*ys',
               order = { })

GC_28 = Coupling(name = 'GC_28',
               value = '-complex(0,1)*EE/MW*Mt/SW/2',
               order = {'QED':1 })

GC_29 = Coupling(name = 'GC_29',
               value = 'complex(0,1)*(Vtb*yb+Vts*ys)',
               order = { })

GC_30 = Coupling(name = 'GC_30',
               value = 'complex(0,1)*(Vub*yb+Vus*ys)',
               order = { })

GC_31 = Coupling(name = 'GC_31',
               value = 'Sqrt2*ym/2',
               order = { })

GC_32 = Coupling(name = 'GC_32',
               value = 'complex(0,1)*EE/3',
               order = {'QED':1 })

GC_33 = Coupling(name = 'GC_33',
               value = '-complex(0,1)/CW*EE*SW/3',
               order = {'QED':1 })

GC_34 = Coupling(name = 'GC_34',
               value = 'complex(0,1)*EE*(SW/CW/6+CW/SW/2)',
               order = {'QED':1 })

GC_35 = Coupling(name = 'GC_35',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vcb/2',
               order = {'QED':1 })

GC_36 = Coupling(name = 'GC_36',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vtb/2',
               order = {'QED':1 })

GC_37 = Coupling(name = 'GC_37',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vub/2',
               order = {'QED':1 })

GC_38 = Coupling(name = 'GC_38',
               value = 'complex(0,1)*EE*(SW/CW/6-CW/SW/2)',
               order = {'QED':1 })

GC_39 = Coupling(name = 'GC_39',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vcd/2',
               order = {'QED':1 })

GC_40 = Coupling(name = 'GC_40',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vcs/2',
               order = {'QED':1 })

GC_41 = Coupling(name = 'GC_41',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vtd/2',
               order = {'QED':1 })

GC_42 = Coupling(name = 'GC_42',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vud/2',
               order = {'QED':1 })

GC_43 = Coupling(name = 'GC_43',
               value = 'complex(0,1)*EE',
               order = {'QED':1 })

GC_44 = Coupling(name = 'GC_44',
               value = 'complex(0,1)*EE*(-SW/CW/2+CW/SW/2)',
               order = {'QED':1 })

GC_45 = Coupling(name = 'GC_45',
               value = '-complex(0,1)/CW*EE*SW',
               order = {'QED':1 })

GC_46 = Coupling(name = 'GC_46',
               value = '-complex(0,1)*EE/SW*Sqrt2/2',
               order = {'QED':1 })

GC_47 = Coupling(name = 'GC_47',
               value = 'complex(0,1)*EE*(-SW/CW/2-CW/SW/2)',
               order = {'QED':1 })

GC_48 = Coupling(name = 'GC_48',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vts/2',
               order = {'QED':1 })

GC_49 = Coupling(name = 'GC_49',
               value = '-complex(0,1)*EE/SW*Sqrt2*Vus/2',
               order = {'QED':1 })

GC_50 = Coupling(name = 'GC_50',
               value = '-complex(0,1)*EE',
               order = {'QED':1 })

GC_51 = Coupling(name = 'GC_51',
               value = 'GG',
               order = {'QCD':1 })

GC_52 = Coupling(name = 'GC_52',
               value = '-complex(0,1)*CW*EE/SW',
               order = {'QED':1 })

GC_53 = Coupling(name = 'GC_53',
               value = 'complex(0,1)*CW*EE/SW',
               order = {'QED':1 })

GC_54 = Coupling(name = 'GC_54',
               value = '-6*complex(0,1)*lamH',
               order = { })

GC_55 = Coupling(name = 'GC_55',
               value = 'complex(0,1)*(-kapHPl+kapHPlp)',
               order = { })

GC_56 = Coupling(name = 'GC_56',
               value = '-complex(0,1)*kapHPq',
               order = { })

GC_57 = Coupling(name = 'GC_57',
               value = 'complex(0,1)*(-kapHPl-kapHPlp)',
               order = { })

GC_58 = Coupling(name = 'GC_58',
               value = 'complex(0,1)*(-6*lamPl-6*lamPlp)',
               order = { })

GC_59 = Coupling(name = 'GC_59',
               value = 'complex(0,1)*(-kapPqPl+kapPqPlp)',
               order = { })

GC_60 = Coupling(name = 'GC_60',
               value = 'complex(0,1)*(-2*lamPl+2*lamPlp)',
               order = { })

GC_61 = Coupling(name = 'GC_61',
               value = '-2*complex(0,1)*lamPq',
               order = { })

GC_62 = Coupling(name = 'GC_62',
               value = 'complex(0,1)*(-kapPqPl-kapPqPlp)',
               order = { })

GC_63 = Coupling(name = 'GC_63',
               value = 'complex(0,1)*(EE*EE)/(SW*SW)/2',
               order = {'QED':2 })

GC_64 = Coupling(name = 'GC_64',
               value = 'complex(0,1)*(EE*EE)*(1+SW*SW/(CW*CW)/2+CW*CW/(SW*SW)/2)',
               order = {'QED':2 })

GC_65 = Coupling(name = 'GC_65',
               value = '8*complex(0,1)*(EE*EE)/9',
               order = {'QED':2 })

GC_66 = Coupling(name = 'GC_66',
               value = '4*complex(0,1)*EE*GG/3',
               order = {'QED':1,'QCD':1 })

GC_67 = Coupling(name = 'GC_67',
               value = '-8*complex(0,1)/CW*(EE*EE)*SW/9',
               order = {'QED':2 })

GC_68 = Coupling(name = 'GC_68',
               value = 'complex(0,1)*(GG*GG)',
               order = {'QCD':2 })

GC_69 = Coupling(name = 'GC_69',
               value = '-4*complex(0,1)/CW*EE*GG*SW/3',
               order = {'QED':1,'QCD':1 })

GC_70 = Coupling(name = 'GC_70',
               value = '8*complex(0,1)/(CW*CW)*(EE*EE)*(SW*SW)/9',
               order = {'QED':2 })

GC_71 = Coupling(name = 'GC_71',
               value = '4*GG*LGGH*Rqcd_h',
               order = {'QCD':1 })

GC_72 = Coupling(name = 'GC_72',
               value = '-4*GG*LGGH*Rqcd_h',
               order = {'QCD':1 })

GC_73 = Coupling(name = 'GC_73',
               value = '-2*complex(0,1)*(EE*EE)',
               order = {'QED':2 })

GC_74 = Coupling(name = 'GC_74',
               value = 'complex(0,1)*(EE*EE)',
               order = {'QED':2 })

GC_75 = Coupling(name = 'GC_75',
               value = '-2*complex(0,1)*CW*(EE*EE)/SW',
               order = {'QED':2 })

GC_76 = Coupling(name = 'GC_76',
               value = 'complex(0,1)*CW*(EE*EE)/SW',
               order = {'QED':2 })

GC_77 = Coupling(name = 'GC_77',
               value = '-complex(0,1)*(GG*GG)',
               order = {'QCD':2 })

GC_78 = Coupling(name = 'GC_78',
               value = '2*complex(0,1)*(EE*EE)/(SW*SW)',
               order = {'QED':2 })

GC_79 = Coupling(name = 'GC_79',
               value = '-complex(0,1)*(EE*EE)/(SW*SW)',
               order = {'QED':2 })

GC_80 = Coupling(name = 'GC_80',
               value = '-2*complex(0,1)*(CW*CW)*(EE*EE)/(SW*SW)',
               order = {'QED':2 })

GC_81 = Coupling(name = 'GC_81',
               value = 'complex(0,1)*(CW*CW)*(EE*EE)/(SW*SW)',
               order = {'QED':2 })

