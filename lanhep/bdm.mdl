%
% Standard Model - unitary and t'Hooft-Feynman gauges.
%
option chepPDWidth=200. % Too many parameters with #>85
keys gauge_fixing=unitary, Fermi=no.

do_if Fermi==yes.
    keys gauge_fixing=unitary.
    model 'Electroweak 4-fermion BDM'/2.
do_else.
do_if gauge_fixing==Feynman.
    model 'B-anomaly DM Model (Feyn. gauge)'/6. %%%
do_else_if gauge_fixing==unitary.
    model 'B-anomaly DM Model (un. gauge)'/1.  %%%
do_else.
    write('Error: the key "gauge" should be either "Feynman" or "unitary".').
    quit.
end_if.
end_if.

option ReduceGamma5=0.
let g5=gamma5.
use sm_tex.

parameter  EE  = 0.31345 : 'Elementary charge (alpha=1/127.9, on-shell, MZ point, PDG2002)',
	       GG  = 1.21358 : 'Strong coupling constant (Z pnt, alp=0.1172\pm0.002  (PDG2002)',
	       SW  = 0.48076 : 'sin of the Weinberg angle (MZ point -> MW=79.958GeV, PDG2002)',
           s12 = 0.2229  : 'Parameter of C-K-M matrix (PDG2002)',
	       s23 = 0.0412  : 'Parameter of C-K-M matrix (PDG2002)',
           s13 = 0.0036  : 'Parameter of C-K-M matrix (PDG2002)',
           MZ  = 91.1876 : 'Z boson mass'.  %%%

parameter  CW  = sqrt(1-SW**2) : 'cos of the Weinberg angle'.

parameter  c12  = sqrt(1-s12**2) : 	'parameter  of C-K-M matrix',
           c23  = sqrt(1-s23**2) : 	'parameter  of C-K-M matrix',
           c13  = sqrt(1-s13**2) : 	'parameter  of C-K-M matrix'.

parameter  Vud = c12*c13 		: 'C-K-M matrix element',
	       Vus = s12*c13 		: 'C-K-M matrix element',
	       Vub = s13     		: 'C-K-M matrix element',
           Vcd = (-s12*c23-c12*s23*s13) : 'C-K-M matrix element',
           Vcs = (c12*c23-s12*s23*s13)  : 'C-K-M matrix element',
	       Vcb = s23*c13 		: 'C-K-M matrix element',
	       Vtd = (s12*s23-c12*c23*s13) 	: 'C-K-M matrix element',
	       Vts = (-c12*s23-s12*c23*s13)	: 'C-K-M matrix element',
	       Vtb = c23*c13  		: 'C-K-M matrix element'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parameter alfSMZ=0.1172 : 'alpha_QCD(MZ)',
          Q=100 : 'renormalization scale',
          Mtp = 173.5 : 'top quark pole mass',
          MbMb = 4.23 : 'Mb(Mb) running mass',
          McMc = 1.27 : 'Mc(Mc) running mass'.

external_func(initQCD5,4).
external_func(McRun,1).
external_func(MbRun,1).
external_func(MtRun,1).
external_func(alphaQCD,1).
external_func(McEff,1).
external_func(MbEff,1).
external_func(MtEff,1).

parameter Lqcd=initQCD5(alfSMZ,McMc,MbMb,Mtp).
parameter Mb=MbEff(Q).
parameter Mt=MtEff(Q).
parameter Mc=McEff(Q).
parameter PI=acos(-1).
parameter Mbp=MbMb*(1+4/3*alphaQCD(MbMb)/PI).
parameter Mcp=Mc*(1+4/3*alphaQCD(Mc)/PI). 

parameter MW = MZ*CW.
parameter vH = 2*MW*SW/EE.   

parameter Mh = 125.0 : 'real SM Higgs mass', 
          MS0 = 70.0 : 'dark matter mass',
          MA = 80.0 : 'pseudoscalar DM mass',
          Mchi = 200.0: 'fermion DM mass',
          MPq = 1000.0: 'colored scalar DM mass'.

parameter lamPq = 0.1: 'Pq self-coupling',
          lamPl = 0.1: 'Pl self-coupling 1',
          lamPlp = 0.1: 'Pl self-coupling 2'.

parameter kapHPq = 1.0e-4: 'Higgs-Pq coupling',
          kapHPl = 0.1: 'Higgs-Pl coupling 1',
          kapPqPl = 1.0e-3: 'Pq-Pl coupling 1',
          kapHPlp = 0.1: 'Higgs-Pl coupling 2',
          kapPqPlp = 1.0e-3: 'Pq-Pl couling 2'.

parameter yb = 0.1: 'b-quark-Pq Yukawa coupling',
          ys = 0.1: 's-quark-Pq Yukawa coupling',
          ym = 0.1: 'mu-Pl Yukawa coupling'.

parameter lamH = Mh**2/(2*vH*vH).

parameter muH2 = Mh**2/2,
          muPl2 = (MS0**2+MA**2-kapHPl*vH*vH)/2,
          muPlp2 = (MS0**2-MA**2-kapHPlp*vH*vH)/2,
          muPq2 = MPq**2 - kapHPq*vH**2/2.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OrthMatrix( { {Vud,Vus,Vub}, {Vcd,Vcs,Vcb}, {Vtd,Vts,Vtb}} ).

do_if Fermi==no.
vector G/G: (gluon, color c8, gauge).
end_if.

do_if gauge_fixing==Feynman.
vector  
	A/A: (photon, gauge),
	Z/Z:('Z boson', mass MZ, width wZ = auto, gauge),
	'W+'/'W-': ('W boson', mass MW, width wW = auto, gauge).

do_else.
vector  A/A: (photon, gauge).
do_if Fermi==no.
vector	Z/Z:('Z boson', mass MZ, width wZ = auto),
	'W+'/'W-': ('W boson', mass MW, width wW = auto).
do_else.
vector	Z/Z:('Z boson', mass MZ, width wZ = auto, '*'),
	'W+'/'W-': ('W boson', mass MW, width wW = auto, '*').
end_if.
end_if.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
spinor '~x-'/'~X+': ('chiC', mass Mchi, width wxC = auto).

spinor '~x0'/'~X0': ('chi0', mass Mchi, width wx0 = auto).

scalar '~pq'/'~Pq': ('Pq', color c3, mass MPq, width wPq = auto). 

scalar '~S'/'~S': ('real Pl', mass MS0),
       '~A'/'~A': ('pseudo Pl', mass MA, width wA = auto).

prtcproperty pdg: ('~S'=900006, '~A'=900007, '~pq'=900008, '~x-' = 900009, '~x0' = 900010).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

spinor 		ne:(neutrino,left), 	   e:(electron, mass Me = 0.000511),
		nm/Nm:('mu-neutrino',left),   m:(muon, mass Mm  = 0.10566),
		nl:('tau-neutrino',left),  l:('tau-lepton', mass Ml  = 1.777).

spinor		u:('u-quark',color c3, mass Mu = 0.0023),
		d:('d-quark',color c3, mass Md = 0.0048),
		c:('c-quark',color c3, mass Mc),
		s:('s-quark',color c3, mass Ms = 0.117),
		t:('t-quark',color c3, mass Mt, width wt = auto),
		b:('b-quark',color c3, mass Mb).

do_if Fermi==no.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
scalar h/h: (Higgs, mass Mh, width wh = auto).   %%%

let H=h.  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end_if.

let l1={ne,e}, L1=anti(l1).
let l2={nm,m}, L2=anti(l2).
let l3={nl,l}, L3=anti(l3).

let q1={u,d}, Q1={U,D}, q1a={u,Vud*d+Vus*s+Vub*b}, Q1a={U,Vud*D+Vus*S+Vub*B}.
let q2={c,s}, Q2={C,S}, q2a={c,Vcd*d+Vcs*s+Vcb*b}, Q2a={C,Vcd*D+Vcs*S+Vcb*B}. 
let q3={t,b}, Q3={T,B}, q3a={t,Vtd*d+Vts*s+Vtb*b}, Q3a={T,Vtd*D+Vts*S+Vtb*B}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
let q3b = {Vub*u + Vcb*c + Vtb*t, b}, Q3b = {Vub*U+Vcb*C+Vtb*T,B}.
let q2b = {Vus*u + Vcs*c + Vts*t, s}, Q2b = {Vus*U+Vcs*C+Vts*T,S}.
let chi = {'~x0','~x-'}, Chi = {'~X0','~X+'}.
let pl = ('~S'+i*'~A')/Sqrt2, Pl = ('~S'-i*'~A')/Sqrt2.
%let pq = '~pq', Pq = '~Pq'.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

let B1= -SW*Z+CW*A, W3=CW*Z+SW*A, W1=('W+'+'W-')/Sqrt2,
	 W2 = i*('W+'-'W-')/Sqrt2.

do_if gauge_fixing==Feynman.

let gh1 = ('W+.c'+'W-.c')/Sqrt2, gh2= i*('W+.c'-'W-.c')/Sqrt2,
		gh3= CW*'Z.c'+SW*'A.c', gh={gh1,gh2,gh3}.

let Gh1 = ('W+.C'+'W-.C')/Sqrt2, Gh2=i*('W+.C'-'W-.C')/Sqrt2, 
		Gh3= CW*'Z.C'+SW*'A.C', Gh={Gh1,Gh2,Gh3}. 

end_if.

let WW1 = {W1,  W2 , W3}, WW = {'W+',W3,'W-'}.

let g=EE/SW, g1=EE/CW.

% Self-interaction of gauge bosons

lterm -F**2/4   where 
	F=deriv^mu*B1^nu-deriv^nu*B1^mu.

do_if Fermi==no.
lterm -F**2/4  where
	F=deriv^mu*G^nu^a-deriv^nu*G^mu^a+i*GG*f_SU3^a^b^c*G^mu^b*G^nu^c.
end_if.

lterm -F**2/4  where
F=deriv^mu*WW1^nu^a-deriv^nu*WW1^mu^a -g*eps^a^b^c*WW1^mu^b*WW1^nu^c.

% left fermion interaction with gauge fields


lterm  	anti(psi)*gamma*(1-g5)/2*(i*deriv-g*taupm*WW/2-Y*g1*B1)*psi
		where 
			psi=l1,  Y=-1/2;
			psi=l2,  Y=-1/2;
			psi=l3,  Y=-1/2;
			psi=q1a, Y= 1/6;
			psi=q2a, Y= 1/6;
			psi=q3a, Y= 1/6.

% right fermion interaction with gauge fields

lterm  	anti(psi)*gamma*(1+g5)/2*(i*deriv - Y*g1*B1)*psi
		where 
			psi=e,Y= -1;
			psi=m,Y= -1;
			psi=l,Y= -1;
			psi=u, Y=  2/3;
			psi=c, Y=  2/3;
			psi=t, Y=  2/3;
			psi=d, Y= -1/3;
			psi=s, Y= -1/3;
			psi=b, Y= -1/3.


% quark-gluon interaction

do_if Fermi==no.
lterm  GG*anti(psi)*lambda*gamma*G*psi where
	psi=q1; psi=q2; psi=q3.
end_if.


do_if Fermi==no.
do_if gauge_fixing==Feynman.

let pp = { -i*'W+.f',  (vev(2*MW/EE*SW)+H+i*'Z.f')/Sqrt2 }, 
    PP = {  i*'W-.f',  (vev(2*MW/EE*SW)+H-i*'Z.f')/Sqrt2 }.

do_else.

let pp = { 0,  (vev(2*MW/EE*SW)+H)/Sqrt2 }, 
    PP = { 0,  (vev(2*MW/EE*SW)+H)/Sqrt2 }.
    
end_if.


lterm  -M/MW/Sqrt2*g*(anti(pl)*(1+g5)/2*pr*pp + anti(pr)*(1-g5)/2*pl*PP )
    where
	M=Vud*0,  pl=q1a, pr=d;          % 0 stands for Md 
	M=Vus*Ms, pl=q1a, pr=s;
	M=Vub*Mb, pl=q1a, pr=b;
	M=Vcd*0,  pl=q2a, pr=d;
	M=Vcs*Ms, pl=q2a, pr=s;
	M=Vcb*Mb, pl=q2a, pr=b;
	M=Vtd*0,  pl=q3a, pr=d;
	M=Vts*Ms, pl=q3a, pr=s;
	M=Vtb*Mb, pl=q3a, pr=b.


lterm  -M/MW/Sqrt2*g*(anti(pl)*(1+g5)/2*i*tau2*pr*PP 
		+ anti(pr)*(1-g5)/2*i*pl*tau2*pp ) 
 where
	M=0 ,  pl=q1a, pr=u;
	M=Mc,  pl=q2a, pr=c;
	M=Mt,pl=q3a, pr=t.

lterm  -M/MW/Sqrt2*g*(anti(pl)*(1+g5)/2*pr*pp + anti(pr)*(1-g5)/2*pl*PP )
    where
	M=Mm,    pl=l2,  pr=m;
	M=Ml,  pl=l3,  pr=l.

%%%%%%%%%%%%%%% fermion interactions %%%%%%%%%%%%
lterm Chi*gamma*(i*deriv-g*taupm*WW/2+g1*B1/2)*chi - Mchi*Chi*chi.
lterm (deriv^mu)*Pl*(deriv^mu)*pl.

let Dpq^mu^a = (deriv^mu+i*2/3*g1*B1^mu)*'~pq'^a - i*GG*lambda^a^b^c*G^mu^c*'~pq'^b.

let DPq^mu^a = (deriv^mu-i*2/3*g1*B1^mu)*'~Pq'^a + i*GG*'~Pq'^b*lambda^b^a^c*G^mu^c.

lterm DPq*Dpq.

lterm yb*Q3b*(1+g5)/2*chi*'~pq'+ys*Q2b*(1+g5)/2*chi*'~pq' + ym*L2*(1+g5)/2*chi*pl + yb*Chi*(1-g5)/2*q3b*'~Pq' + ys*Chi*(1-g5)/2*q2b*'~Pq' + ym*Chi*(1-g5)/2*l2*Pl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	

%%%%%%%%%%%%%%%% Higgs potential   %%%%%%%%%%%%%%%%%%%
%lterm -2*lambda*(pp*PP-v**2/2)**2  where 
%	lambda=(g*MH/MW)**2/16, v=2*MW*SW/EE.


lterm muH2*PP*pp - muPl2*Pl*pl - muPq2*'~Pq'*'~pq' - muPlp2*(Pl*Pl+pl*pl)/2
      -lamH*(pp*PP)**2 - lamPq*('~Pq'*'~pq')*('~Pq'*'~pq') - lamPl*(Pl*pl)**2 
      -kapHPq*(PP*pp)*('~Pq'*'~pq') - kapHPl*(PP*pp)*(Pl*pl) - kapPqPl*('~Pq'*'~pq')*(Pl*pl)
      - lamPlp*(Pl*Pl+pl*pl)**2/4 - kapPqPlp*('~Pq'*'~pq')*(Pl*Pl+pl*pl)/2 
      - kapHPlp*(PP*pp)*(Pl*Pl+pl*pl)/2.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



let Dpp^mu^a = (deriv^mu+i*g1/2*B1^mu)*pp^a +
	 i*g/2*taupm^a^b^c*WW^mu^c*pp^b.

let DPP^mu^a = (deriv^mu-i*g1/2*B1^mu)*PP^a 
	-i*g/2*taupm^a^b^c*{'W-'^mu,W3^mu,'W+'^mu}^c*PP^b.

	

lterm DPP*Dpp.


lterm -i*GG*f_SU3*ccghost(G)*G^mu*deriv^mu*ghost(G).
lterm  -1/2*(deriv*G)**2.

end_if.

do_if gauge_fixing==Feynman.

%lterm -g*eps*gh*WW1*deriv*Gh.

lterm g*eps*deriv*Gh*gh*WW1.


lterm  -1/2*(deriv*A)**2.


lterm  -1/2*(2*(deriv*'W+'+MW*'W+.f')*(deriv*'W-'+MW*'W-.f') +
	(deriv*Z+MW/CW*'Z.f')**2).


lterm -MW*EE/2/SW*((H+i*'Z.f')*('W-.C'*'W+.c' + 'W+.C'*'W-.c')
    		+H*'Z.C'*'Z.c'/CW**2-2*i*'Z.f'*'W+.C'*'W-.c').

lterm i*EE*MW/2/CW/SW*(
	'W+.f'*('W-.C'*'Z.c'*(1-2*SW**2)+'W-.c'*'Z.C'
			+2*CW*SW*'W-.C'*'A.c') -
	'W-.f'*('W+.C'*'Z.c'*(1-2*SW**2)+'W+.c'*'Z.C'
			+2*CW*SW*'W+.C'*'A.c')).
end_if.

%%%%%%%%%%%%%%%%%
SetAngle(1-SW**2=CW**2).
read 'higgs_eff1.lhp'.
%%%%%%%%%%%%%%%%%

SetEM(A,EE).

CheckHerm.

CheckMasses.






