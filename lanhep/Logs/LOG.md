## The color problem in vertices.py debugging (LONG)
I tried to convert the standard model given bby lanhep (without ufo, stand.mdl) and qcd.mdl and qed.mdl and they all worked (after deleting those extra aS params in standard).
Funnily not the stand_ufo.mdl which probably was supposed to.


Now when I tried our model (besides the extra aS) it also complained of color. This is the debug from madgraph:

```
            #************************************************************
            #*                     MadGraph5_aMC@NLO                    *
            #*                                                          *
            #*                *                       *                 *
            #*                  *        * *        *                   *
            #*                    * * * * 5 * * * *                     *
            #*                  *        * *        *                   *
            #*                *                       *                 *
            #*                                                          *
            #*                                                          *
            #*         VERSION 2.7.3                 2020-06-21         *
            #*                                                          *
            #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
            #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
            #*                                                          *
            #************************************************************
            #*                                                          *
            #*               Command File for MadGraph5_aMC@NLO         *
            #*                                                          *
            #*     run as ./bin/mg5_aMC  filename                       *
            #*                                                          *
            #************************************************************
            set default_unset_couplings 99
            set group_subprocesses Auto
            set ignore_six_quark_processes False
            set loop_optimized_output True
            set loop_color_flows False
            set gauge unitary
            set complex_mass_scheme False
            set max_npoint_for_channel 0
            import model sm
            define p = g u c d s u~ c~ d~ s~
            define j = g u c d s u~ c~ d~ s~
            define l+ = e+ mu+
            define l- = e- mu-
            define vl = ve vm vt
            define vl~ = ve~ vm~ vt~
            import model bdmUFO/
            Traceback (most recent call last):
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/madgraph/interface/extended_cmd.py", line 1515, in onecmd
                return self.onecmd_orig(line, **opt)
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/madgraph/interface/extended_cmd.py", line 1464, in onecmd_orig
                return func(arg, **opt)
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/madgraph/interface/master_interface.py", line 280, in do_import
                self.cmd.do_import(self, *args, **opts)
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/madgraph/interface/madgraph_interface.py", line 5412, in do_import
                complex_mass_scheme=self.options['complex_mass_scheme'])
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/models/import_ufo.py", line 239, in import_model
                model = import_full_model(model_path, decay, prefix)
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/models/import_ufo.py", line 385, in import_full_model
                model = ufo2mg5_converter.load_model()
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/models/import_ufo.py", line 548, in load_model
                self.add_interaction(interaction_info, color_info)
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/models/import_ufo.py", line 1317, in add_interaction
                for color_obj in interaction_info.color]
              File "/home/daniboy/madgraph/MG5_aMC_v2_7_3/models/import_ufo.py", line 1535, in treat_color
                for data in output if data !='1'])
              File "<string>", line 1
                color.T(3,1)color.T(4,2)
                                ^
            SyntaxError: invalid syntax
            Related File: <string>
                                      MadGraph5_aMC@NLO Options    
                                      ----------------    
                    complex_mass_scheme 	:	False
                default_unset_couplings 	:	99
                                  gauge 	:	unitary
                     group_subprocesses 	:	Auto
              ignore_six_quark_processes 	:	False
                       loop_color_flows 	:	False
                  loop_optimized_output 	:	True
              low_mem_multicore_nlo_generation 	:	False
                 max_npoint_for_channel 	:	0
                           stdout_level 	:	20 (user set)

                                     MadEvent Options    
                                      ----------------    
                 automatic_html_opening 	:	True
                                nb_core 	:	None
                    notification_center 	:	True
                               run_mode 	:	2

                                  Configuration Options    
                                  ---------------------    
                                    OLP 	:	MadLoop
                                amcfast 	:	amcfast-config
                               applgrid 	:	applgrid-config
                            auto_update 	:	7
                     cluster_local_path 	:	None
                       cluster_nb_retry 	:	1
                          cluster_queue 	:	None (user set)
                     cluster_retry_wait 	:	300
                           cluster_size 	:	100
                  cluster_status_update 	:	(600, 30)
                      cluster_temp_path 	:	None
                           cluster_type 	:	condor
                                collier 	:	./HEPTools/lib
                           cpp_compiler 	:	None
                         crash_on_error 	:	False
                           delphes_path 	:	./Delphes
                             eps_viewer 	:	None
                    exrootanalysis_path 	:	./ExRootAnalysis
                          f2py_compiler 	:	None
                                fastjet 	:	None (user set)
                       fortran_compiler 	:	None
                                  golem 	:	None (user set)
                             hepmc_path 	:	None (user set)
                              hwpp_path 	:	None (user set)
                                 lhapdf 	:	lhapdf-config
                      madanalysis5_path 	:	None (user set)
                       madanalysis_path 	:	./MadAnalysis
              mg5amc_py8_interface_path 	:	None (user set)
                                  ninja 	:	./HEPTools/lib
                    output_dependencies 	:	external
                        pythia-pgs_path 	:	./pythia-pgs
                           pythia8_path 	:	None (user set)
                                samurai 	:	None
                           syscalc_path 	:	./SysCalc
                                td_path 	:	./td
                            text_editor 	:	None
                            thepeg_path 	:	None (user set)
                                timeout 	:	60
                            web_browser 	:	None

```

since treat_color in import_ufo.py from madgraph was complaining i did some printing at the problematic lines:
```
...
output = data_string.split('*')
print output                        <<<< ADDDED BY ME TO PRINT output
output = color.ColorString([eval(data) \
                            for data in output if data !='1'])
print 'yay'
...
```
that last line is where it breaks and it never gets to yay. this is the output:
```
...
['color.T(4,3)']
yay
['1']
yay
['color.T(3,1)color.T(4,2)']
```
I wager that it was expecting a '*' between the to colors because of the first line.  output = data_string.split('*')
So in the end it fails to convert the color to ColorString

LETS INVESTIGATE ColorString

I searched the madgraph directory and found this line to confirm the format needed:    
    tests/unit_tests/iolibs/test_export_v4.py:                      
```
'color': [color.ColorString([color.T(1,0),color.T(3,2)])],
```
clearly the fact that there are two colors is not a problem. the problem was failing to separate them like this
```
['color.T(3,1)color.T(4,2)'] -> ['color.T(3,1)','color.T(4,2)']
```
which after eval(data) in the last line would turn it to the same as the example i found
```
['color.T(3,1)','color.T(4,2)'] -> [color.T(3,1),color.T(4,2)]
```


!!!! ColorString is a class found in madgraph/madgraph/core/color_algebra.py
also note that the files that do the importing and exporting of ufo are in madgraph/madgraph/iolibs
```
class ColorString(list):
"""A list of ColorObjects with an implicit multiplication between,
together with a Fraction coefficient and a tag
to indicate if the coefficient is real or imaginary. ColorStrings can be
simplified, by simplifying their elements."""
```
Trying to trace where ColorString fails exactly:
    __init__ added a print but it didn't run, strange.
    it seems it fails before that. tried to add the following line:
        *print [eval(data) for data in output if data !='1']*
    but that already failed. tested it it's the for that fails by using
        *print [data for data in output if data !='1']*
    and it produced correctly as expected one element with no splitting:
        *['color.T(3,1)color.T(4,2)']*
    thus eval fails when applied to this. i tested the following two cases to make sure its this
        *print eval('color.T(3,1)')*
        *print eval('color.T(3,1)color.T(4,2)')*
    the first one printed T(3,1) while the second failed as expected


SO FOR NOW I can say don't blame eval or ColorString. Let's climb up the ranks and see what treat_color does.

treat_color has data_string as parameter. this is what is split using '*' and this is done right at the beggining without altering data_string hich leaves me to conclude that it's already assumed by this point the '*'
```
        def treat_color(self, data_string, interaction_info, color_info):
        """ convert the string to ColorString"""
        (...) <<< no data_string calls
        for term in data_string.split('*'):
```
however this line might be the one that was supposed to add it :
```
        (...) <<< large code for some checks and rewrites data_string with output
        data_string = '*'.join(output)
```
lets print before and after to check what the code inbetween does:
```
        print 'treat_color start:'
        print data_string
        for term in data_string.split('*'):
            (...) << a lot of stuff and output creation
        data_string = '*'.join(output)
        print 'data_string verified and altered maybe:'
        print data_string
```
this produces:
```
        treat_color start:
        Identity(1,3)Identity(2,4)
        data_string verified and altered maybe:
        color.T(3,1)color.T(4,2)
```
which means it was this part of that big code that changed Identity to color.T (note that it uses interaction_info and color_info given to treat_colors for the pdg):
```
        if color_info[particle.pdg_code] == 3 :
            output.append(self._pat_id.sub('color.T(\g<second>,\g<first>)', term))
        elif color_info[particle.pdg_code] == -3:
            output.append(self._pat_id.sub('color.T(\g<first>,\g<second>)', term))
```
Either way Im pretty sure all the code already fails over here:
        for term in data_string.split('*'):
since it doesnt split properly and term evaluates to the full Identity(1,3)Identity(2,4).
funnily I think the code doesn't break here since the line 
        pattern = self._pat_id.search(term) 
doesn't break which it probably should had but it probably stops once it finds the first Identity. and then the line
         output.append(self._pat_id.sub('color.T(\g<first>,\g<second>)', term))
also per chance works even if there's more then once color.Idenity (unless they did meant it to be this way, who knows)




Let's keep climbing up the ranks. but before that let's print all the problematic parameters treat_color got:
    data_string = 
        Identity(1,3)Identity(2,4)
    interaction_info = 
        V_103
    color_info = 
        {1: -3, 2: -3, 3: -3, 4: -3, 5: -3, 6: -3, 900008: -3, -1: 3, -900008: 3, -6: 3, -5: 3, -4: 3, -3: 3, -2: 3}
note that interaction_info has more stuff but the print only does this

treat_color is called by add_interaction
```
        def add_interaction(self, interaction_info, color_info, type='base', loop_particles=None):            
            """add an interaction in the MG5 model. interaction_info is the 
            UFO vertices information."""
        (...)
            colors = [self.treat_color(color_obj, interaction_info, color_info) 
                                    for color_obj in interaction_info.color]
```
color_obj is the terms in interaction_info.color and it prints to ['Identity(1,3)Identity(2,4)', 'Identity(1,4)Identity(2,3)']
from what I see add_interaction doesn't change interaction_info so lets go up.

add_interaction get's called in load_model(self).
It's called in two places, directly here:
        for interaction_info in self.ufomodel.all_vertices:
            self.add_interaction(interaction_info, color_info)
or indirectly by going through add_CTinteraction:
        for interaction_info in self.ufomodel.all_CTvertices:
            self.add_CTinteraction(interaction_info, color_info)
but I checked and it goes dierctly.

printinf self.ufomodel.all_vertices just gives all the vertices [V_1, ... V_119] (which checks out with vertices.py from model)
all_vertices no longer appears in this file but by now im pretty confident that only treat_color get it's hand dirty and changes vertice and the rest is just an unfiltered pipeline of the vertices. But we can check the vertice V_103 (in vertices.py in the ufo model) to see if it already started like this:
```
        V_103 = Vertex(name='V_103',
              particles = [  P.__tilde__Pq,  P.__tilde__Pq,  P.__tilde__pq,  P.__tilde__pq ],
              lorentz = [ L.SSSS_1  ],
              color = [ 'Identity(1,3)Identity(2,4)', 'Identity(1,4)Identity(2,3)' ],
              couplings = {(0,0):C.GC_61,(1,0):C.GC_61})
```
So clearly it already starts like this and  the problem is not madgraphs assuming it as suposed to have '*'.
I'm going to try to find a model that has something like this with two Identities to see if they use '*'.

In madgraph's sm i could only find one instance of multiplication but not the Identity:
```
        color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
```
and the same is for the standUFO I made with lanhep.
The only place I did find Identity*Identity was in madgraph's loop_sm in CT_vertices.py. note it's not normal vertices (also i checked the model works)
```
        color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
```

ALSO in the bdm model we do have this:
              color = [ '2f(1,2,-1)*f(3,4,-1)2',
    so im pretty sure Identity should have as well.




FINALLY now I'm happy saying that the problem is definetly with lanhep or the model who didn't include the '*' between colors since one of madgraph's model has that in vertices and I couldn't find any model without '*'.
To solve this I clearly prefer to change lanhep since they are still testing the ufo exporter. Let's see if I can find where to do this or if its a problem with the model files.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Btw the vertice that gives the problem was:
```
        V_103 = Vertex(name='V_103',
              particles = [  P.__tilde__Pq,  P.__tilde__Pq,  P.__tilde__pq,  P.__tilde__pq ],
              lorentz = [ L.SSSS_1  ],
              color = [ 'Identity(1,3)Identity(2,4)', 'Identity(1,4)Identity(2,3)' ],
              couplings = {(0,0):C.GC_61,(1,0):C.GC_61})
```
              
and in the model Pq is the Phi_q field with color defined here:
        scalar '~pq'/'~Pq': ('Pq', color c3, mass MPq, width wPq = auto). 
but I'm not sure about P, the only thing that might be related to it is:
```
        do_if Fermi==no.
        do_if gauge_fixing==Feynman.
        let pp = { -i*'W+.f',  (vev(2*MW/EE*SW)+H+i*'Z.f')/Sqrt2 }, 
            PP = {  i*'W-.f',  (vev(2*MW/EE*SW)+H-i*'Z.f')/Sqrt2 }.
            
        do_else.
        let pp = { 0,  (vev(2*MW/EE*SW)+H)/Sqrt2 }, 
            PP = { 0,  (vev(2*MW/EE*SW)+H)/Sqrt2 }.
            
        and in huggs_eff1.lhp:
        _p=[c,b,t,l] in   parameter ahF__p=CoefVrt([anti(_p),_p,h]) /(mass _p).
```
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Well I can't find Identity in the model file or vertices so clearly lanhep is to blame.
Let's go dive into another source code, yay.

Searching for 'vertices.py' we find that lagr.c is the one that annouces that it writes it, and it calls UF_write_lagr:
```
	if(UFOutput==0)
	{
		FA_write_lagr(l,f);
		fclose(f);
	}
	else
		UF_write_lagr(l);
```
this is defined in wrtuf.c:
```
    void UF_write_lagr(List l)
    { (...) }
```

and I found that the lines that print the color part is 772 to 781
```
		fprintf(f,"              color = [");
		for(l2=lc;l2;l2=ListTail(l2))
		{
			fprintf(f," '");
			wrt_expr(f,NewInteger(1),0,CopyTerm(ListFirst(l2)));
			fprintf(f,"'");
			if(ListTail(l2))
				fprintf(f,",");
		}
		fprintf(f," ],\n");
```
		
And each term is the line: wrt_expr(f,NewInteger(1),0,CopyTerm(ListFirst(l2)));
so even deeper we must go on our quest!

This code makes me want to vomit. The best I can figure out is that line 1238 does the printing of each Identity
			sno+=fprintf(of,"Identity(%d,%d)",il1+1,il2+1);
But it seems that they hardcoded the f*f cases thus I guess if I want to fix it I should do it the same way. But it's so ugly im not sure I want to. Ill try it manually for now.























## Wrapping it all up

### Conversion from mdl to ufo check

Will first check if the parameters defined in the **bdm_ufo.mdl** are correctly translated into **parameters.py**.
Commands that create entries in **parameters.py** are:
- *parameter x = value/formula*
- when defining particles if the mass or width was not defined previously it will also generate params *Z/Z:('Z boson', mass MZ = 123, width wZ = auto, gauge)*
- *widths* are defined as *zero* by default
- Don't really understand the external_func but it seems to create some variables. For example **initQCD5** created only **initQCD51**
    I think it creates one variable for every call of the function. Not sure if it's per call or per parameter given to the function.

Differences and possible problems that appeared from conversion
- **GG** get's translated to *internal* to be calculated using:

```python
2*cmath.sqrt(cmath.acos(-1)*aS)
```

instead of the direct *GG = 1.21358* as defined in **bdm_ufo.mdl**.
        
The formula is correct though since gS=sqrt(4*pi*aS)
This also makes one of the three **aS** that appear since it's used in this formula.

- When creating fields the params also get an entry in **parameters.py**. However I need to check if setting the width to auto will correctly set up madgraph to calculate it automatically afterwards since in **parameters.py** there's nothing indicating this.
    Searched the whole *UFO* folder with grep and found no mentions of *auto* so it doesn't seem like it.
- Maybe not important but for example *Mc* is defined as an internal with value *McEff1* which in turn is defined as external with value 0.66065
This is defined in **bdm.mdl**
    
```python
external_func(McEff,1).
...
parameter Mc=McEff(Q).
```
        
and appears in **parameters.py** as
     
```python
McEff1 = Parameter(name = 'McEff1',
   nature = 'external', (...)
   value = 0.66065,
...
Mc = Parameter(name = 'Mc',
   nature = 'internal',
   value = 'McEff1',
```
        
- in the model there's *alphaQCD(MbMb)* and *alphaQCD(Mc)* which I was expecting having different values but in **parameters.py** both of those (*AlphaQCD1* and *AlphaQCD2*) have the same 0.1156 value. Also this happens with *alphaQCD3*.
- same as the previous point *hGGeven1* is equal to simply 1 where I expected maybe something more from *hGGeven(Mh,aQCD_h,3, 1,3,Mbp, ahF_b, 1,3,Mcp, ahF_c, 1,3,Mtp,ahF_t)*. same to be said of all the *hXXevenN* params as they are all 1. also *initQCD5*. There's probably more.
- *CoefVrt* seems to work correctly maybe since it gives a formula and it ws different for the different couplings. Here's one example:

```python
parameter a_hV_W=CoefVrt(['W+','W-', h]) /MW**2.
```
```python
a_hV_W = Parameter(name = 'a_hV_W', (...)
   value = 'EE*MW/SW/(MW*MW)',
```
        
- There's a definition of a **Zero** and **Sqrt2** parameter but it's probably not a problem since they are to be used internally. (als sm from madgraph also has the zero)
- **Aparently** lanhep seems to decide what is external or internal not just based on if they have a formula or not. Since I tried defining *Gf* with a formula but placed it before some of its parameters were defined and it wanted to make *Gf* external. I placed it at the end of the file and it no longar complained. I also tested by placing it right before and after that param was defined and it did behave as I thought.

### Comparison with sm from madgraph check

- Maybe the masses from *ymX* in **sm** are not exactly the same but there is effective stuff involved so that might be the difference. Not sure.

At a quick glance of the ufo files it seems there's no glaring issues except the mentioned. I'm going to check at the madgraph level aka the param cards generated.


### aEWM1 and Gf
It complains that *aEWM1* and *GF* are not defined: 
```
WARNING:  aEWM1 and Gf not define in MODEL. AQED will not be written correcty in LHE FILE 
```
Thus I will add a part that defines these to the model but only if converting to ufo ( lanhep has an example of this with *stand_ufo.lhep* in *mdl* folder ).
Unfortunately I'm going to have to make these external and I guess I'll follow the *stand_ufo.mdl* way basically.

**OK** I think it's fixed now. No more complains or warnings.

### Masses PDG conflicts
So McMc and McEff1 both have the same PDG. Question is can I jsut give them a different one?

### Masses and Width not defined as proper Param objects
I was gettin a weird compilation error and it seems the **particles.py** is generated wrong. We have in the masses and widths just strings like this:
```python
t =  Particle(  pdg_code = 6, (...)
              mass = 'Mt',
              width = 'wt',
```
When I went and checked other models it should look something like this:
```python
t =  Particle(  pdg_code = 6, (...)
              mass = Param.Mt,
              width = Param.wt,
```
note that *Param.Mt* comes from the actual name of the variable in **parameters.py**:
```python
Mt = Parameter(...)
```
Not sure how to fix this but note that in the test directory of lanhep it has the same issue. Thus it's probably a code problem.

I decided for now to manually change them using a script. Check the **Readme.md** for details



## baba process checking
### Check of param and run cards (taken from Events directly) by comparing with sm

- I had forgotten to set widths to auto thus they are all zero but this should'nt affect the result of cross section


- Some differences that might be important:

| var | bdm | SM | Note |
|--|--|--|--|
| aEWM1 | 128 | 132.507 | |
| mass w+ | 79.83833 | 80.419 | this is dependent so no need to change | |
| mass z | 91.1876 | 91.188 | |
| MbEff1 / Mb | 3.2407 | 4.7 | note that theres also MbMb=4.23 param but i'm ignoring it for now |
| MtEff1 / Mt | 171.4 | 173 | Mtp=173.5 |
| McEff1 / Mc | 0.66065 | 0 | McMc = 1.27 |
| Me | 0.000511 | 0 | |
| Mm | 0.10566 | 0 | |
| Md | 0.0048 | 0 | |
| Mu | 0.0023 | 0 | |
| Ms | 0.117 | 0 | |

The following parameters are left for each:

```
BDM
Block couplings 
    1 1.000000e-01 # lamPq 
    2 1.000000e-01 # lamPl 
    3 1.000000e-01 # lamPlp 
    4 1.000000e-04 # kapHPq 
    5 1.000000e-01 # kapHPl 
    6 1.000000e-03 # kapPqPl 
    7 1.000000e-01 # kapHPlp 
    8 1.000000e-03 # kapPqPlp 
    
Block mass 
  900006 7.000000e+01 # MS0 
  900007 8.000000e+01 # MA 
  900008 1.000000e+03 # MPq 
  900009 2.000000e+02 # Mchi 
## Dependent parameters,
  900010 2.000000e+02 # ~x0 : Mchi 

Block misc 
    1 1.000000e+00 # hGGeven1 
    2 1.000000e+00 # hAAeven1 
    3 1.000000e+00 # hAAeven2 
    4 1.000000e+00 # hAAeven3 

Block sminputs 
    5 1.172000e-01 # alfSMZ 
    6 1.000000e+02 # Q 
    7 1.000000e+00 # initQCD51 
   11 1.156000e-01 # alphaQCD1 
   12 1.156000e-01 # alphaQCD2 
   13 1.156000e-01 # alphaQCD3 

Block yukawa 
    1 1.000000e-01 # yb 
    2 1.000000e-01 # ys 
    3 1.000000e-01 # ym 

Block ckm 
    1 2.229000e-01 # s12 
    2 4.120000e-02 # s23 
    3 3.600000e-03 # s13 
```

```
SM
Block yukawa 
    5 4.700000e+00 # ymb 
    6 1.730000e+02 # ymt 
   15 1.777000e+00 # ymtau 
```

- I don't understand the electric charge. It looks like they are all zero?
```
Block QNUMBERS 900009  # ~x- 
        1 0  # 3 times electric charge
        2 2  # number of spin states (2S+1)
        3 1  # colour rep (1: singlet, 3: triplet, 8: octet)
        4 1  # Particle/Antiparticle distinction (0=own anti)
Block QNUMBERS 900010  # ~x0 
        1 0  # 3 times electric charge
        2 2  # number of spin states (2S+1)
        3 1  # colour rep (1: singlet, 3: triplet, 8: octet)
        4 1  # Particle/Antiparticle distinction (0=own anti)
Block QNUMBERS 900008  # ~pq 
        1 0  # 3 times electric charge
        2 1  # number of spin states (2S+1)
        3 3  # colour rep (1: singlet, 3: triplet, 8: octet)
        4 1  # Particle/Antiparticle distinction (0=own anti)
Block QNUMBERS 900006  # ~S 
        1 0  # 3 times electric charge
        2 1  # number of spin states (2S+1)
        3 1  # colour rep (1: singlet, 3: triplet, 8: octet)
        4 0  # Particle/Antiparticle distinction (0=own anti)
Block QNUMBERS 900007  # ~A 
        1 0  # 3 times electric charge
        2 1  # number of spin states (2S+1)
        3 1  # colour rep (1: singlet, 3: triplet, 8: octet)
        4 0  # Particle/Antiparticle distinction (0=own anti)
```



## Compare with the CalcHEP results
## Send bugs to Russian
## pedir ao rodrigo o paper que ele disse ao tomas (76 ou algo assim) sobre a fenomologia






# Cross Sections not coming out as expected





